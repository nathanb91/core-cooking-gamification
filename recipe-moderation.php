<?php
	
	/*

	Author: Nathan Brettell
	Digital Media Project
	Gamification


	*/

	include('header.php');
?>
		<div id="main">
			<div id="main-background"></div>

			<div id="content">
				<article id="post">
					<h1> Thank you for submitting a recipe.</h1>

					<p> Your recipe will be reviewed by administration before going live, please bear with us. You will recieve an email once your recipe has been approved.</p>

				<div class="large-button light-blue">
					<a href="/core/posts"><span>Continue Journey</span></a>
				</div>

				</article>
			</div>
		</div>

<?php include('footer.html'); ?>
