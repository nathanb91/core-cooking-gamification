<nav id="nav" class="navigation" role="navigation">
	<ul class="menu">
		<li class="nav-link"> Browse
			<ul class="submenu" style="display: none;">
				<li><a href="/core/posts">View all</a></li>
				<li><a id="popular" href="#">Popular</a></li>
				<li><a id="quick_simple" href="#">Quick and Simple</a></li>
			</ul>
		</li>
	</ul>
</nav>
