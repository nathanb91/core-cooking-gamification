<?php
	
	/*

	Author: Nathan Brettell
	Digital Media Project
	Gamification


	*/

	include('header.php');
?>
<div id="main">
	<div id="content">
		<article id="post-directory">

		<h2 class="search-title"> You have searched for <span><?php echo $_POST['search-form']; ?></span> </h2>
			<?php

				$recipe_search = $g->recipe_search($_POST['search-form']);

				//print_r($recipe_search);

				foreach($recipe_search as $recipe_result) {

				$description = substr($recipe_result['post_content'], 0, 120);

			?>

			<div id="post-entry-<?php echo $recipe_result['post_id'] ?>" class="post-entry">

			<span class="exp level-<?php echo $recipe_result['post_level'];?>"><?php echo $recipe_result['post_experience']; ?> xp - Level <?php echo $recipe_result['post_level'];?></span>

			<a href="post/<?php echo $recipe_result['post_slug'];?>">
				<div class="photo_thumb"><img src="/core/timthumb.php?src=<?php echo $recipe_result['post_photo']; ?>&q=80&w=220" /></div>
			</a>
				<div class="post-container">
					<!--<span class="post_date"><?php echo $recipe_searchDate; ?></span>-->
					<h6 class="post-title"><a href="post/<?php echo $recipe_result['post_slug'];?>"><?php echo $recipe_result['post_title']; ?></a></h6>
					<span class="post_author">Created by: <a href="/core/profile/<?php echo $recipe_result['author']; ?>"><?php echo $recipe_result['author']; ?></a></span>
										
					<div class="post_category"> Posted in <a href="posts/category/<?php echo $cat['category_slug']; ?>"><?php echo $cat['category_name']; ?></a></div>

					<!-- limit to certain amount of words with elipsis (...) -->
					<p><?php echo $description.'...'; ?></p>
					<?php $ratings = $g->get_rating($recipe_result['post_id']); ?>
					<span class="star"><?php echo substr($ratings['avg'], 0 , 3); ?></span>

				</div>
			</div>
			<?php } //end foreach ?>
		</article>
	</div>
</div>

<?php include('footer.html'); ?>