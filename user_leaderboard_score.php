<?php
/************************************************************* 
 * This script is developed by Arturs Sosins aka ar2rsawseen, http://webcodingeasy.com 
 * Feel free to distribute and modify code, but keep reference to its creator 
 * 
 * Gamify class allows to implement game logic into PHP aplications. 
 * It can create needed tables for storing information on most popular database platforms using PDO. 
 * It also can add users, define levels and achievements and generate user statistics and tops.
 * Then it is posible to bind class functions to user actions, to allow them gain experience and achievements.
 * 
 * For more information, examples and online documentation visit:  
 * http://webcodingeasy.com/PHP-classes/Implement-game-logic-to-your-web-application
**************************************************************/
//include("header.php");
include("./gamify/gamify.php");
$g = new gamify("SERVER", "USERNAME", "PASSWORD", "DATABASE");
//$g = new gamify("localhost", "root", "root", "gamify");

$g->debug();

	$users = $g->user_leaderboard($_POST['userid'], $_POST['userid'], $_POST['userid']);

//$user = $_GET['user'];
echo "<table id='user-leaderboard' border='0' cellpadding='5' cellspacing='5' style='width: 100%;'>";
echo "<tbody>";
echo "<tr><th>";
if(isset($_GET["sort"]) && $_GET["sort"] == "playername" && !isset($_GET["desc"]))
{
	echo "<a class='sort' href='/core/profile/".$user."&sort=username&desc=true'>Username</a>";
}
else
{
	echo "<a class='sort' href='/core/profile/".$user."&sort=playername'>Username</a>";
}
echo "</th><th>";
if(isset($_GET["sort"]) && $_GET["sort"] == "experience" && !isset($_GET["desc"]))
{
	echo "<a class='sort' href='/core/profile/".$user."&sort=experience&desc=true'>Experience</a>";
}
else
{
	echo "<a class='sort' href='/core/profile/".$user."&sort=experience'>Experience</a>";
}
echo "</th></tr>";
//echo "</th><th>level</th><th>options</th></tr>";
//print_r($_GET);
foreach($users as $user)
{

?>
	<tr id="<?php echo $user['userID']; ?>" class="user-details">
		<td class='username'><?php echo $user["playername"]; ?></td>
		<td class='experience'><?php echo $user["experience"]; ?></td>
		<td class='level'><?php echo $user["level"]; ?></td>
	</tr>

<?php	
}
?>
</tbody>
</table>