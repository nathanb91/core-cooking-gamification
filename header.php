<?php

session_name('gamify_login');
	
session_set_cookie_params(2*7*24*60*60);
// Making the cookie live for 2 weeks


session_start();
if(!empty($_POST['password']) && ($_POST['submit']=='Login')) {
	header('Location: /core/profile/'.$_POST['username']);
	//exit();
}

// Define base url
//define('BASE_URL', '/DMP/Core/');



	if(isset($_GET['op']) && ($_GET['op'] == 'logout')) {
		$_SESSION = array();
		session_destroy();
		header('Location: index.php');
		exit;
	}

	//load gamify class library into application
	include("./gamify/gamify.php");
	$g = new gamify("SERVER", "USERNAME", "PASSWORD", "DATABASE");

?>
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
	
	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<!--[if IE ]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<![endif]-->

	<?php 
			//get the filename and display as the page title.
			$url = $_SERVER['REQUEST_URI']; 
			$title = basename($url, ".php");

			$title = ucfirst($title);
	?>

	<title> The Core || <?php echo $title; ?></title>

	<meta name="title" content="Gamification || <?php echo $title; ?> ">

	<meta name="description" content="Gamification Website" />
	<!--Google will often use this as its description of your page/site. Make it good.-->

	<meta name="author" content="Nathan Brettell" />

	<meta name="viewport" content="width=device-width" />
	
	<link rel="shortcut icon" href="/core/images/core-logo.png" />
	<link rel="apple-touch-icon" href="favicon.png" />

	<!-- stylesheets -->
	<link rel="stylesheet" href="/core/css/reset.css" />
	<link rel="stylesheet" href="/core/css/style.css" />
	<link rel="stylesheet" href="/core/js/superslides/dist/stylesheets/superslides.css" />

	<!-- jQuery Load -->
	<!--<script type='text/javascript' src='/core/js/jquery.min.js'></script>-->
	<script src="http://code.jquery.com/jquery-2.0.3.js"></script>
	<script type='text/javascript' src='/core/js/script.js'></script>

	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script src="/core/js/flyto.min.js"></script>

	<!-- This is an un-minified, complete version of Modernizr. 
		 Before you move to production, you should generate a custom build that only has the detects you need.
	<script src="/js/modernizr-2.6.2.dev.js"></script> -->

</head>
<body class="not-front">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=164520836967694";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="page-wrapper">
<header id="header" role="header">
	
		<!--<h1> <a href="/core">C <span id="logo"><img src="/core/images/core-apple-icon.png" /></span> re</a> </h1>-->
		<h1> <a href="/core"> <span id="logo"><img src="/core/timthumb.php?src=/core/images/core-logo.png&q=100&h=80" /></span> </a></h1>
		<!--<h6> - Fun with food - </h6>-->

	<section id="searchForm" class="search">
	<form action="/core/search" method="POST" name="search">
		<input type="text" class="search-input" name="search-form" />
		<input type="submit" value="Go" />
	</form>
	</section>
	<?php include('navigation.php'); ?>

		<?php

	if(empty($_SESSION['player_name'])) {
	//not logged in
				
	?>
		<section id="playerNotActive" class="player">
			<div class="background"></div>
			<a href="#2" id="openLogin">Login</a>
			<a href="/core/registration" class="register">Create an account</a>
		</section>

	<?php }//endif ?>

	<?php require_once('login_form.php'); ?>
	
</header>