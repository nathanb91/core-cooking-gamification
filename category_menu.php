<?php

	$category_menu = $g->get_categories();
	$count = 0;

	//echo 'category menu ' . print_r($category_menu, true);
	echo "<aside id='sidebar_menu'>";
		echo "<div id='category_menu'>";
			echo "<ul id='category-navigation' class='menu'>"; 
				foreach($category_menu as $cat_menu) {
					//print_r($cat_menu);
					//help from http://stackoverflow.com/questions/8540141/add-different-class-to-even-and-odd-divs to acheive odd and even clases
					echo "<li id='cat-" . $cat_menu['category_id'] . "' class='category_item ".(++$count%2 ? "odd" : "even")."'><a href='/core/posts/category/" . $cat_menu['category_slug'] . "'>" . $cat_menu['category_name'] . "</a></li>";
				}
				if($_SESSION['player_id']) {
					echo "<li id='create-your-own' class='category_item'><a href='/core/create-your-own'>+ Create your own recipe</a></li>";
				}
			echo "</ul>";
		echo "</div>";
	echo "</aside>";

?>