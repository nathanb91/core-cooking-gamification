<?php
	
	/*

	Author: Nathan Brettell
	Digital Media Project
	Gamification


	*/

	include('header.php');

	$level = $playerDetails['level'];

	if(empty($playerDetails)) {
		$level = 9;
	}

?>
		<div id="main">

			<?php include('category_menu.php'); ?>

			<div id="content">

			
				<article id="post-directory">

					<?php 

					$g->debug();

					$cat_slug = $_GET['id'];

					$category = $g->get_category($cat_slug);

					echo "<h1>" . $category['category_name'] . " </h1>";

					//echo print_r($category, true);
					//print_r($playerDetails);

					$cat_id = $category['category_id'];

					$catPosts = $g->category_posts($cat_id, $level);

					//echo print_r($catPosts, true);
					if(count($catPosts) > 0) {
						foreach($catPosts as $catPost) {

							//$category = $g->post_category($post['post_id']);

							//echo 'category ' . print_r($category, true);
							$post_date = $post['post_date'];
							$postDate = date('l j F o', strtotime($post_date));

							$description = substr($catPost['post_content'], 0, 120);

							?>

							<div id="post-entry-<?php echo $catPost['post_id'] ?>" class="post-entry">
								<?php //print_r($post); ?>
							<span class="exp level-<?php echo $catPost['post_level'];?>"><?php echo $catPost['post_experience']; ?> xp - Level <?php echo $catPost['post_level'];?></span>
									
								<a href="/core/post/<?php echo $catPost['post_slug'];?>">
										<div class="photo_thumb"><img src="/core/timthumb.php?src=<?php echo $catPost['post_photo']; ?>&q=80&w=220" /></div>
								</a>
									<div class="post-container">
										<h6 class="post-title"><a href="/core/post/<?php echo $catPost['post_slug'];?>"><?php echo $catPost['post_title']; ?></a></h6>
										<span class="post_author">Created by: <a href="/core/profile/<?php echo $catPost['author']; ?>"><?php echo $catPost['author']; ?></a></span>
																				
										<!--<div class="post_category"> Posted in <a href="posts/category/<?php echo $catPost['category_slug']; ?>"><?php echo $catPost['category_name']; ?></a></div>-->

										<!-- limit to certain amount of words with elipsis (...) -->
										<p><?php echo $description.'...'; ?></p>
										<?php $ratings = $g->get_rating($post['post_id']); ?>
										<span class="star"><?php echo substr($ratings['avg'], 0 , 3); ?></span>

									</div>
								</div>


							<?php /*
								//echo "<h1><a href='post.php?id=".$post['post_id'].">Post Title: " . $post['post_title'] . '</a></h1>';
								echo "<div id='post-entry-".$catPost['post_id']." class='post-entry'>";
									echo "<h4 class='post-title'><a href='/core/post/".$catPost['post_slug']."'>" . $catPost['post_title'] . "</a></h4>";

									//echo print_r($catPost, true);
									echo '<div class="photo_thumb"><img src=/core/timthumb.php?src=' . $catPost['post_photo'] . '&q=80&w=220 /></div>';
									echo "<span class='post_date'>" . $postDate . "</span>";
									//echo "<div class='post_category'> Posted in <a href=post/" . $cat['category_slug'] . ">" . $cat['category_name'] . "</a></div>";
									echo "<p>" . $catPost['post_content'] . "</p>";
								echo "</div>";
								*/
							}
					} else {
						echo "<h1> No Posts within this category </h1>";
					}

					?>
				</article>
			</div>
		</div>

<?php include('footer.html'); ?>