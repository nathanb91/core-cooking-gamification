<?php
	
	/*

	Author: Nathan Brettell
	Digital Media Project
	Gamification

	Player profile


	*/

	include('header.php');

	$player = $_GET['user'];

	# check to make sure user exists

	$player_stats = $g->get_user($player);
	//$player_stats = true;
	//print_r($player_stats);

	//echo print_r($g->get_errors(), true);

	$error = $g->get_errors();

	//echo '<pre>' . print_r($player_stats, true) . '</pre>'; 

?>

		<div id="main" class="profile-area">
			<div id="content">
			<div class="display_error"><span><?php echo $error['0']; ?></span></div>
				
					<?php echo $error['0']; ?>
					<?php if(count($error) < 1) { ?>
						<h2 class="profile-header">Profile of <?php echo $player; ?></h2>
							<?php } else { ?>
						<h2>Profile doesn't exist</h2>
					<?php } ?>

					<section id="player-area">	
							<form enctype="multipart/form-data" name="avatar_upload" id="user-form" action="" method="post">
						
							<span class="image-src" style="display: none;">No file chosen</span>
								<div id="upload-area">
								<!-- Blank avatar source - http://cdn.crowdfundinsider.com/shared/avatars/blank_avatar.png -->
								<img id="user-avatar" src="<?php echo $player_stats['avatar_source']; ?>" />							
									<span class="upload-button">Upload</span>
									<input type="file" name="avatar-field" id="avatar-upload" class="form-upload" />	
								</div>

								<input type="submit" class="text-button" name="submit" value="Upload" />
							</form>
						<div class="player_stats">
							<h3> Level <?php echo $player_stats['level']; ?> </h3>
							<span> Experience points: <?php echo $player_stats['experience']; ?> </span>
						</div>

					</section>

			<?php
			//print_r($_FILES);
			//print_r($_POST);
			if($_POST['submit'] == 'Upload') {
				$upload_directory = __DIR__ . '/images/avatars/';

				$avatar_upload = $upload_directory . $_FILES['avatar-field']['name'];

				$avatar_image = '/core/images/avatars/' . $_FILES['avatar-field']['name'];
				//echo $avatar_image;

				//upload the file to the upload directory
				move_uploaded_file($_FILES['avatar-field']['tmp_name'], $avatar_upload);

				$g->upload_avatar($player_stats['userID'], $avatar_image);
			}

			?>
			
			<div class="segment-left">
				<section id="achievements">
					<h5> Achievements </h5>
					<?php 
						# get user achievements
						$achievements = $player_stats['achievements'];

						foreach ($achievements as $achievement) {
						//echo print_r($achievement, true);

						$player_achievement = $g->get_achievement($achievement['achID']);
						?>

							
							<div class="achievement">
								<h6 class="achievement-title"><?php echo $achievement['achievement_name'] ?></h6>
								<img class="achiement-badge" src="/core/achievements/<?php echo $achievement['badge_src'] ?> " />
								<div class="achievement-description"><?php echo $player_achievement['description']  ?></div>
							</div>
					<?php

						}


					?>
					</section>
				</div>
					
				<div class="segment-right">
					<div class="player-leaderboard">
						<h5> Top 10 Players </h5>

						<div id="loading"><img src="../images/ajax-loading.gif" alt="ajax load animation" /></div>

						<section id="leaderboard-update" class="leaderboard">
						</section>

					</div>

					<div class="player-leaderboard">
						<h5> Rank </h5>

						<!--<div id="user_loading"><img src="../images/ajax-loading.gif" alt="ajax load animation" /></div>-->

						<section id="user-leaderboard-update" class="leaderboard">
						</section>

					</div>
				</div>

				<section id="recipe_history">
					<h4> Completed Recipes </h4>
					<?php 

					$recipe_history = $g->recipe_history($player_stats['userID']); 

					//print_r($recipe_history);

					if(is_array($recipe_history)) {


					foreach($recipe_history as $recipe) {

					$description = substr($recipe['post_content'], 0, 120);

					?>
						<div id="post-entry-<?php echo $recipe['post_id'] ?>" class="post-entry level-<?php echo $recipe['post_level']; ?>">
						<span class="exp level-<?php echo $recipe['post_level'];?>"><?php echo $recipe['post_experience']; ?> xp - Level <?php echo $recipe['post_level'];?></span>

								<a href="/core/post/<?php echo $recipe['post_slug'];?>">
								<?php //print_r($recipe_history); ?>

								<div class="photo_thumb"><img src="/core/timthumb.php?src=<?php echo $recipe['post_photo']; ?>&q=80&w=220" /></div>
								</a>

									<div class="post-container">
										<!--<span class="post_date"><?php echo $recipe_historyDate; ?></span>-->
										<h6 class="post-title"><a href="/core/post/<?php echo $recipe_history['post_slug'];?>"><?php echo $recipe['post_title']; ?></a></h6>
										<span class="post_author">Created by: <a href="/core/profile/<?php echo $recipe['author']; ?>"><?php echo $recipe['author']; ?></a></span>
										
										<div class="post_category"> No Category has been allocated. </div>

										<!-- limit to certain amount of words with elipsis (...) -->
										<p><?php echo $description.'...'; ?></p>
										<?php $ratings = $g->get_rating($recipe['post_id']); ?>
										<span class="star"><?php echo substr($ratings['avg'], 0 , 3); ?></span>

									</div>
								</div>
					<?php 
						}
					} 
					?>
					</section>
			</div>
		</div>
<script>
$(document).ready(function($) {

	var dataRate = 'sort=<?php echo $_GET['sort']; ?>&desc=<?php echo $_GET['desc']; ?>&user=<?php echo $_GET['user']; ?>';
	$.ajax({
    	type : "GET",
    	url : "/core/leaderboard_score.php",
    	data: dataRate,
    	success:function(data){
    		$('#leaderboard-update').append(data);

    		$('table#leaderboard tr.user-details').click(function() {

				$(this).next('tr.achievements').slideToggle("slow");
				$(this).toggleClass('active-row');
			});
    	}
    });

    var dataRate = 'userid=<?php echo $player_stats['userID']; ?>';
	$.ajax({
    	type : "POST",
    	url : "/core/user_leaderboard_score.php",
    	data: dataRate,
    	success:function(data){
    		$('#user-leaderboard-update').append(data);

    		//add current class to the user profile in the leaderboard
    		$('#<?php echo $player_stats['userID']; ?>').addClass('current');
    	}
    });

    $('#loading')
    .hide()
    .ajaxStart(function() {
        $(this).show();
    })
    .ajaxStop(function() {
        $(this).hide();
    });


		//help from http://jsfiddle.net/LvsYc/
		function readURL(input) {
	        if (input.files && input.files[0]) {
	            var reader = new FileReader();
	            
	            reader.onload = function (e) {
	                $('#user-avatar').attr('src', e.target.result);
	            }

	            	$('span.image-src').text(input.files[0].name);
	            	console.log(input.files);
	            
	            reader.readAsDataURL(input.files[0]);
	        }
	    }
	    
	    $("#avatar-upload").change(function(){
	        readURL(this);
	    });
   
});
</script>

<?php include('footer.html'); ?>
