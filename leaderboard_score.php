<?php
/************************************************************* 
 * This script is developed by Arturs Sosins aka ar2rsawseen, http://webcodingeasy.com 
 * Feel free to distribute and modify code, but keep reference to its creator 
 * 
 * Gamify class allows to implement game logic into PHP aplications. 
 * It can create needed tables for storing information on most popular database platforms using PDO. 
 * It also can add users, define levels and achievements and generate user statistics and tops.
 * Then it is posible to bind class functions to user actions, to allow them gain experience and achievements.
 * 
 * For more information, examples and online documentation visit:  
 * http://webcodingeasy.com/PHP-classes/Implement-game-logic-to-your-web-application
**************************************************************/
//include("header.php");
include("./gamify/gamify.php");
$g = new gamify("SERVER", "USERNAME", "PASSWORD", "DATABASE");
//$g = new gamify("localhost", "root", "root", "gamify");

$g->debug();
if(isset($_GET["sort"]))
{
	if(isset($_GET["desc"]))
	{
		$users = $g->leaderboard($_GET["sort"], true);
	}
	else
	{
		$users = $g->leaderboard($_GET["sort"]);
	}
}
else
{
	$users = $g->leaderboard();
}

//echo print_r($users, true);
//print_r($_GET);
$user = $_GET['user'];
echo "<table id='leaderboard' border='0' cellpadding='5' cellspacing='5'>";
echo "<tbody>";
echo "<tr><th>";
if(isset($_GET["sort"]) && $_GET["sort"] == "playername" && !isset($_GET["desc"]))
{
	echo "<a class='sort' href='/core/profile/".$user."&sort=username&desc=true'>Username</a>";
}
else
{
	echo "<a class='sort' href='/core/profile/".$user."&sort=playername'>Username</a>";
}
echo "</th><th>";
if(isset($_GET["sort"]) && $_GET["sort"] == "experience" && !isset($_GET["desc"]))
{
	echo "<a class='sort' href='/core/profile/".$user."&sort=experience&desc=true'>Experience</a>";
}
else
{
	echo "<a class='sort' href='/core/profile/".$user."&sort=experience'>Experience</a>";
}
echo "</th></tr>";
//echo "</th><th>level</th><th>options</th></tr>";
//print_r($_GET);

foreach($users as $user)
{

?>
	<tr id="<?php echo $user['ID']; ?>" class="user-details">
		<td class='username'><?php echo $user["playername"]; ?></td>
		<td class='experience'><?php echo $user["experience"]; ?></td>
		<td class='level'><?php echo $user["level"]; ?></td>
	</tr>
	<tr class='achievements'>
		<td class='user-info' colspan='4'>
		<?php 
		$info = $g->get_user($user["playername"]);
		//echo print_r($info, true);
		?>
		<span>Information about <?php echo $info["username"]; ?></span>
		<p>Experience: <?php echo $info["experience"]; ?></p>
		<p>Level: <?php echo $info["level"]; ?></p>
		<div id='user-achievements'>Achievements: <ul>
			
			<?php foreach($info["achievements"] as $val)
			{
				if($val["status"] == "completed")
				{
				?>
					<li><?php echo $val["achievement_name"]; ?><br/>Badge: <img src='/core/achievements/<?php echo $val["badge_src"]?>' width='50px' border='0'/><br/>Earned : <?php echo date("r", $val["time"]); ?></li>
				
				<?php
				}
			}
			?>
			</ul>
		</div>
		</td>
	</tr>

<?php	
}
?>
</tbody>
</table>