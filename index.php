<?php
	
/*

	Author: Nathan Brettell
	Digital Media Project
	Gamification
	
	Photos Courtesy of http://www.noelbarnhurst.com/portfolio/
	Icons http://thenounproject.com/search and http://www.freepik.com/


*/

//include('header.php');

session_name('gamify_login');
	
session_set_cookie_params(2*7*24*60*60);
// Making the cookie live for 2 weeks

session_start();
if(empty($err) && ($_POST['submit']=='Login')) {
	header('Location: /core/profile/'.$_POST['username']);
	//exit();
}
// Define base url
//define('BASE_URL', '/DMP/Core/');


	if(isset($_GET['op']) && ($_GET['op'] == 'logout')) {
		$_SESSION = array();
		session_destroy();
		header('Location: index.php');
		exit;
	}

	//load gamify class library into application
	include("./gamify/gamify.php");
	$g = new gamify("SERVER", "USERNAME", "PASSWORD", "DATABASE");

?>

<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
	
	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<!--[if IE ]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<![endif]-->

	<?php 
			//get the filename and display as the page title.
			$url = $_SERVER['REQUEST_URI']; 
			$title = basename($url, ".php");

			$title = ucfirst($title);
	?>

	<title> The Core || <?php echo $title; ?></title>

	<meta name="title" content="Gamification || <?php echo $title; ?> ">

	<meta name="description" content="Gamification Website" />
	<!--Google will often use this as its description of your page/site. Make it good.-->

	<meta name="author" content="Nathan Brettell" />

	<meta name="viewport" content="width=device-width" />
	
	<link rel="shortcut icon" href="/core/images/core-logo.png" />
	<link rel="apple-touch-icon" href="favicon.png" />

	<!-- stylesheets -->
	<link rel="stylesheet" type="text/css" href="/core/css/jquery-ui.css" />
	<link rel="stylesheet" href="/core/css/reset.css" />
	<link rel="stylesheet" href="/core/css/style.css" />
	<link rel="stylesheet" href="/core/js/superslides/dist/stylesheets/superslides.css" />

	<link href='http://fonts.googleapis.com/css?family=Allura' rel='stylesheet' type='text/css'>

	<!-- jQuery Load -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="/core/js/jquery-1.10.2.js"></script>

  	<!-- JavaScript -->
  	<script src="/core/js/superslides/examples/javascripts/jquery.easing.1.3.js"></script>
  	<script src="/core/js/superslides/examples/javascripts/jquery.animate-enhanced.min.js"></script>
	<script type='text/javascript' src='/core/js/superslides/examples/javascripts/hammer.min.js'></script>
  	<script src="/core/js/superslides/dist/jquery.superslides.js" type="text/javascript" charset="utf-8"></script>

	<!-- This is an un-minified, complete version of Modernizr. 
		 Before you move to production, you should generate a custom build that only has the detects you need.
	<script src="/js/modernizr-2.6.2.dev.js"></script> -->

</head>
<body class="front">


<div id="slides">
  
  <ul class="slides-container">
  <li>
	  <header id="header" class="slide-header" role="header">
		<div class="slide-wrapper">
		<?php

			if(empty($_SESSION['player_name'])) {
			//not logged in
				
			?>
			<section id="playerNotActive" class="player">
				<a href="#2" id="openLogin">Sign in</a>
				<a href="registration" class="register highlight">Create an account</a>
			</section>

			<?php }//endif ?>

			<?php

			if(!empty($_SESSION['player_name'])) {
			//logged in
				$player_name = $_SESSION['player_name'];

			//get user level
			$playerDetails = $g->get_user($player_name);

			$next_level = $g->get_next_level($playerDetails['level']);

			$level_percentage = (($playerDetails['experience'] / $next_level['experience_needed']) * 100);

			?>

			<section id="playerActive" class="player">
				<!--<div class="user_experience"> Experience <span><?php echo $playerDetails['experience']; ?></span> </div>-->
				<div class="user_level"> <span>Level</span> <?php echo $playerDetails['level']; ?> <?php echo $playerDetails['level_name']; ?> </div>
				<div class="level_bar" style="width: 220px; height: 20px;"> <div class="level-bar-fill" style=" width: <?php echo $level_percentage; ?>%;"><span class="percentage"><?php echo round($level_percentage); ?>%</span></div> </div>
				<span> <a id="player" href="#"><?php echo $player_name; ?></a> 
					<ul id="user-links" style="display: none;">
						<li><a href="/core/profile/<?php echo $player_name; ?>"> Profile </a></li>
						<li><a href="?op=logout">Log out</a></li>
					</ul>
				</span>
			</section>

			<?php }//endif ?>
		<hgroup>
			<!--<h1> <a href="/core">C <span id="logo"><img src="/core/images/core-apple-icon.png" /></span> re</a> </h1>-->
			<h1> <a href="/core"> <span id="logo"><img class="preserve" src="/core/timthumb.php?src=/core/images/core-logo.png&q=100&h=95" /></span> </a></h1>
			<h6> Play with your food </h6>
		</hgroup>

		<?php //include('navigation.php'); ?>
		<div class="description">
			<h2 class="desc-title"><a href="/core/posts">Discover...</a></h2>

				<section id="search_form_container">
			    	<form action="/core/search" method="post" id="search_form">
			    	<!-- Search for: Pasta, Cream, Fudge... -->
			    		<input type="text" value="Search for: Pasta, Cream, Fudge..." rel="Search for: Pasta, Cream, Fudge..." id="search-bar" name="search-form" class="form-control txt-auto" />
			    	</form>
			    </section>

			<p> Learn how to create amazing looking food, make use of ingredients you didn't know existed. </p>		

			<div class="intro-button blue">
				<a href="/core/how-it-works">Find Out More</a>
			</div>
		</div>

		</div>
	</header>
      <img class="back" src="/core/images/slideshow/NB_american_burger_23241.jpg" width="1024" height="683" alt="Cinelli">
  </li>
  <li>
  <header class="slide-header">
  	<div class="slide-wrapper">
  		<hgroup>
			<!--<h1> <a href="/core">C <span id="logo"><img src="/core/images/core-apple-icon.png" /></span> re</a> </h1>-->
			<h1> <a href="/core#1"> <span id="logo"><img class="preserve" src="/core/timthumb.php?src=/core/images/core-logo.png&q=100&h=95" /></span> </a></h1>
			<h6> Play with your food </h6>
		</hgroup>

		<?php require_once('login_form.php'); ?>

		<div class="intro-button green">
			<a href="#1">&#171; Go Back</a>
		</div>
	</div>
  </header>
  	  
      <img class="back" src="/core/images/slideshow/NB_crab_pizzetta_3692.jpg" width="1024" height="683" alt="Cinelli">
   </li>
   <li>
   	<header class="slide-header">
   		<div class="slide-wrapper">
	   		<hgroup>
				<!--<h1> <a href="/core">C <span id="logo"><img src="/core/images/core-apple-icon.png" /></span> re</a> </h1>-->
				<h1> <a href="/core#1"> <span id="logo"><img class="preserve" src="/core/timthumb.php?src=/core/images/core-logo.png&q=100&h=95" /></span> </a></h1>
				<h6> Play with your food </h6>
			</hgroup>
	   		<h3>Ready?</h3>
	   		<div class="description">
				<p> Start now, increase your learning curve and enjoy cooking! </p>
			</div>

			<div class="intro-button red">
				<a href="/core/posts">Get Started</a>
			</div>
		</div>
	</header>

      <img class="back" src="/core/images/slideshow/NB_peach_melba__10051.jpg" width="1024" height="683" alt="Cinelli">
   </li>
  </ul>

  </div>
	
	<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

	<script src="/core/js/jquery.ui.autocomplete.html.js"></script>

	<script type='text/javascript' src='/core/js/script.js'></script>

  <script>
    $(function() {

     var $slides = $('#slides');

      Hammer($slides[0]).on("swipeleft", function(e) {
        $slides.data('superslides').animate('next');
      });

      Hammer($slides[0]).on("swiperight", function(e) {
        $slides.data('superslides').animate('prev');
      });

      $slides.superslides({
        hashchange: true
      });
    });
  </script>

  <script type="text/javascript">
   	$(function() {

   	var posts = <?php echo $g->ajax_search($_GET['term']); ?>;
   	//console.log(posts);

   	//http://www.pontikis.net/blog/jquery-ui-autocomplete-step-by-step
 
    $("#search-bar").autocomplete({
        source: posts,
        minLength: 2,
        select: function(event, ui) {
            var url = ui.item.id;
            if(url != '#') {
                location.href = '/core/post/' + url;
            }
        },
 
        //html: true, // optional (jquery.ui.autocomplete.html.js required)
 
      // optional (if other layers overlap autocomplete list)
        open: function(event, ui) {
            $(".ui-autocomplete").css("z-index", 1000);
        }
    	});
	});
   	 </script>

   	 <script type="text/javascript">
   	 	$(function() {

   	 		//http://stuff.nekhbet.ro/2010/11/14/jquery-search-box-with-default-text.html

   	 		$('#search-bar').on('blur', function() {
   	 			var default_val = $(this).attr("rel");

   	 			if($(this).val() == "") {
   	 				$(this).val(default_val);
   	 			}
   	 		}).on('focus', function() {
   	 			var default_val = $(this).val();

   	 			if($(this).val() == default_val) {
   	 				$(this).val("");
   	 			}
   	 		});
   	 	});

   	 </script>
 </body>

<?php include('footer.html'); ?>
