<?php
require '../core/gamify/gamify.php';
//load gamify class library into application
$g = new gamify("SERVER", "USERNAME", "PASSWORD", "DATABASE");

    if($_POST['act'] == 'popular'){
    	//search if the user(ip) has already gave a note
        print_r($_POST);
    	$rating = $_POST['rate'];
        $user = $_POST['user'];
    	$post_id = $_POST['post_id'];

         $popular_posts = $g->get_popular_posts($levelID);
         print_r($popular_posts);
    } 
?>


<?php
	
	/*

	Author: Nathan Brettell
	Digital Media Project
	Gamification


	*/

	include('header.php');

	$level = $playerDetails['level'];

	if(empty($playerDetails)) {
		$level = 9;
	}

?>
		<div id="main">

			<?php include('category_menu.php'); ?>

			<div id="content">

	    <!-- Function to count amount of recipes -->
	    <?php $all_posts = $g->get_posts($level); ?>
		<span class="recipe-count">Browse through <?php echo count($all_posts); ?> recipes</span>
		<div>difficulty search</div>
		<div>level search/filtering</div>

				<article id="post-directory">

				<div class="post-container">

					<?php $posts = $g->get_posts($level);

					//echo print_r($posts, true);
					$count = 0;

						foreach($posts as $post) {
							//echo print_r($post, true);

							$count++;

							/*if($count % 5 == 0) {
								echo '<div class="divider"></div>';
							}*/

							$g->debug();
							$category = $g->post_category($post['post_id']);

							//echo 'category ' . print_r($category, true);
							$post_date = $post['post_date'];
							$postDate = date('l j F o', strtotime($post_date));

							$description = substr($post['post_content'], 0, 120);

							if($category) {
								foreach($category as $cat) {
								//echo 'category ' . print_r($cat, true);
							?>
								<!--<h1><a href='post.php?id=".$post['post_id'].">Post Title: " . $post['post_title'] . '</a></h1>-->
								<div id="post-entry-<?php echo $post['post_id'] ?>" class="post-entry level-<?php echo $post['post_level'];?>">
								<?php //print_r($post); ?>
								<span class="exp level-<?php echo $post['post_level'];?>"><?php echo $post['post_experience']; ?> xp - Level <?php echo $post['post_level'];?></span>

								<a href="post/<?php echo $post['post_slug'];?>">
									<div class="photo_thumb"><img src="/core/timthumb.php?src=<?php echo $post['post_photo']; ?>&q=80&w=220" /></div>
								</a>
									<div class="post-container">
										<!--<span class="post_date"><?php echo $postDate; ?></span>-->
										<h6 class="post-title"><a href="post/<?php echo $post['post_slug'];?>"><?php echo $post['post_title']; ?></a></h6>
										<span class="post_author">Created by: <a href="/core/profile/<?php echo $post['author']; ?>"><?php echo $post['author']; ?></a></span>
										
										<div class="post_category"> Posted in <a href="posts/category/<?php echo $cat['category_slug']; ?>"><?php echo $cat['category_name']; ?></a></div>

										<!-- limit to certain amount of words with elipsis (...) -->
										<p><?php echo $description.'...'; ?></p>
									</div>
								</div>
							<?php
									}
								}

								else

								{
							?>

							<!--<h1><a href='post.php?id=".$post['post_id'].">Post Title: " . $post['post_title'] . '</a></h1>-->
							
								<div id="post-entry-<?php echo $post['post_id'] ?>" class="post-entry">
								<a href="post/<?php echo $post['post_slug'];?>">
								<?php //print_r($post); ?>
								<span class="exp"><?php echo $post['post_experience']; ?> xp</span>

								<div class="photo_thumb"><img src="/core/timthumb.php?src=<?php echo $post['post_photo']; ?>&q=80&w=220" /></div>
								</a>

									<div class="post-container">
										<!--<span class="post_date"><?php echo $postDate; ?></span>-->
										<h6 class="post-title"><a href="post/<?php echo $post['post_slug'];?>"><?php echo $post['post_title']; ?></a></h6>
										<span class="post_author">Created by: <a href="/core/profile/<?php echo $post['author']; ?>"><?php echo $post['author']; ?></a></span>
										
										<div class="post_category"> No Category has been allocated. </div>

										<!-- limit to certain amount of words with elipsis (...) -->
										<p><?php echo $post['post_content']; ?></p>
									</div>
								</div>
							
							
							<?php
							}

							
						}

					?>
					</div>
					
				</article>

				<!--<div class="new_post"><a href="/core/create-your-own" class="button"> + Create new post </a></div>-->
			</div>
		</div>

<?php include('footer.html'); ?>
