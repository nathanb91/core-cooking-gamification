<?php
	
	/*

	Author: Nathan Brettell
	Digital Media Project
	Gamification


	*/

	include('header.php');

	$level = $playerDetails['level'];

	if(empty($playerDetails)) {
		$level = 9;
	}

?>
		<div id="main">

			<?php include('category_menu.php'); ?>

			<div id="content">

			<div id="content_top">
			    <!-- Function to count amount of recipes -->
			    <?php $all_posts = $g->get_posts($level); ?>
				<span class="recipe-count">Browse through <?php echo count($all_posts); ?> recipes</span>
				
				<?php $difficulty_options = $g->get_difficulty(); ?>
				<ul id="difficulty-field" class="form-select" name="difficulty">
				<label for="difficulty" class="form-label">Difficulty </label>
					<?php foreach($difficulty_options as $difficulty) { ?>
						<li id="<?php echo $difficulty['difficulty_id']; ?>"><?php echo $difficulty['difficulty_name']; ?></li>
					<?php } ?>
				</ul>

				<?php $level_options = $g->get_levels(); ?>
				<ul id="level-field" class="form-select" name="level">
				<label for="level" class="form-label">Level </label>
					<?php foreach($level_options as $level_value) { ?>
						<li id="<?php echo $level_value['ID']; ?>"><?php echo $level_value['level_name']; ?></li>
					<?php } ?>
				</ul>
			</div>

				<article id="post-directory">

				<div class="post-container">

					<?php $posts = $g->get_posts($level);

					//echo print_r($posts, true);
					$count = 0;

						foreach($posts as $post) {
							//echo print_r($post, true);

							$count++;

							/*if($count % 5 == 0) {
								echo '<div class="divider"></div>';
							}*/

							$g->debug();
							$category = $g->post_category($post['post_id']);

							//echo 'category ' . print_r($category, true);
							$post_date = $post['post_date'];
							$postDate = date('l j F o', strtotime($post_date));

							$description = substr($post['post_content'], 0, 120);

							if($category) {
								foreach($category as $cat) {
								//echo 'category ' . print_r($cat, true);
							?>
								<!--<h1><a href='post.php?id=".$post['post_id'].">Post Title: " . $post['post_title'] . '</a></h1>-->
								<div id="post-entry-<?php echo $post['post_id'] ?>" class="post-entry level-<?php echo $post['post_level'];?>">
								<?php //print_r($post); ?>
								<span class="exp level-<?php echo $post['post_level'];?>"><?php echo $post['post_experience']; ?> xp - Level <?php echo $post['post_level'];?></span>

								<a href="post/<?php echo $post['post_slug'];?>">
									<div class="photo_thumb"><img src="/core/timthumb.php?src=<?php echo $post['post_photo']; ?>&q=80&w=220" /></div>
								</a>
									<div class="post-container">
										<!--<span class="post_date"><?php echo $postDate; ?></span>-->
										<h6 class="post-title"><a href="post/<?php echo $post['post_slug'];?>"><?php echo $post['post_title']; ?></a></h6>
										<span class="post_author">Created by: <a href="/core/profile/<?php echo $post['author']; ?>"><?php echo $post['author']; ?></a></span>
										
										<div class="post_category"> Posted in <a href="posts/category/<?php echo $cat['category_slug']; ?>"><?php echo $cat['category_name']; ?></a></div>

										<!-- limit to certain amount of words with elipsis (...) -->
										<p><?php echo $description.'...'; ?></p>
										<?php $ratings = $g->get_rating($post['post_id']); ?>
										<span class="star"><?php echo substr($ratings['avg'], 0 , 3); ?></span>
									</div>
								</div>
							<?php
									}
								}

								else

								{
							?>

							<!--<h1><a href='post.php?id=".$post['post_id'].">Post Title: " . $post['post_title'] . '</a></h1>-->
							
								<div id="post-entry-<?php echo $post['post_id'] ?>" class="post-entry">
								<a href="post/<?php echo $post['post_slug'];?>">
								<?php //print_r($post); ?>
								<span class="exp"><?php echo $post['post_experience']; ?> xp</span>

								<div class="photo_thumb"><img src="/core/timthumb.php?src=<?php echo $post['post_photo']; ?>&q=80&w=220" /></div>
								</a>

									<div class="post-container">
										<!--<span class="post_date"><?php echo $postDate; ?></span>-->
										<h6 class="post-title"><a href="post/<?php echo $post['post_slug'];?>"><?php echo $post['post_title']; ?></a></h6>
										<span class="post_author">Created by: <a href="/core/profile/<?php echo $post['author']; ?>"><?php echo $post['author']; ?></a></span>
										
										<div class="post_category"> No Category has been allocated. </div>

										<!-- limit to certain amount of words with elipsis (...) -->
										<p><?php echo $post['post_content']; ?></p>
									</div>
								</div>
							
							
							<?php
							}

							
						}

					?>
					</div>
					<script>
					$(function() {
					$('#difficulty-field, #level-field').find('li').hide();
					$('#difficulty-field label, #level-field label').click(function() {
						$(this).parent().find('li').slideToggle();
						$(this).addClass('selected');
					});

					$(document).mouseup(function (e)
						{
						    var filter = $('#difficulty-field label, #level-field label');

						    if (!filter.is(e.target) // if the target of the click isn't the container...
						        && filter.has(e.target).length === 0) // ... nor a descendant of the container
						    {
						    	$('#difficulty-field li, #level-field li').slideUp(600);
						    } 

						     	$('#difficulty-field label, #level-field label').removeClass('hidden'); 
						});

					var filter;
					var difficulty;
					var level;
					$('#difficulty-field li').click(function() {
						filter = 'difficulty';
						level = '';
						difficulty = $(this).attr('id');
					});

					$('#level-field li').click(function() {
						filter = 'level';
						difficulty = '';
						level = $(this).attr('id');
						console.log(level);
					});

					$('.form-select li').click(function() {
						//console.log(filter);
						var dataRate = 'act=filter&filter='+filter+'&difficulty='+difficulty+'&level='+level; //
			                $.ajax({
			                    type : "POST",
			                    url : "/core/ajax_filter_posts.php",
			                    data: dataRate,
			                    success:function(data){
			                   		$('#post-directory').html(data);
			                    }
			                });
			            });
					});
					</script>
					
				</article>

				<!--<div class="new_post"><a href="/core/create-your-own" class="button"> + Create new post </a></div>-->
			</div>
		</div>

<?php include('footer.html'); ?>
