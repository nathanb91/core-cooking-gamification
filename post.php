<?php
	
	/*

	Author: Nathan Brettell
	Digital Media Project
	Gamification


	*/

	include('header.php');
?>
		<div id="main">
			<div id="content">
				<article id="post">

					<?php
					//var_dump($_SERVER);
					//echo print_r($_GET, true);

					//echo $g->debug();

					// retrieve individual post
					$post = $g->view_post($_GET['id']);
					//print_r($post);

					$author = $post['author'];

					$post_date = $post['post_date'];
					$postDate = date('l j F o', strtotime($post_date));


					//$check_achievement = $g->check_achievement($playerDetails['userID'], $post['category_id']);


					//$finish = $g->complete_recipe($playerDetails['userID'], $post['post_id']);
					//$achievement_check = $g->check_achievement($playerDetails['userID']);
					//$achievement = $g->action($playerDetails['username'], $check_achievement['ID'], 1);
					//print_r($check_achievement);
					//print_r($playerDetails);
					//echo $playerDetails['level'];

				?>

					<div id="post-entry-<?php echo $post['post_id']; ?>" class="post-entry single-recipe">		
					<div class="social-area">
						<h6> Share this recipe! </h6>
						<div class="fb-share-button" data-href="http://nathanbrettell.com/core/<?php echo $post['post_slug']; ?>" data-type="button_count"></div>
						<div class="fb-like" data-href="http://nathanbrettell.com/core/<?php echo $post['post_slug']; ?>" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
					</div>
					<div id="recipe-wrapper">
						<div id="recipe_features">	
							<div class="recipe-photo"><img src="<?php echo $post['post_photo']; ?>"></div>
							<div class="recipe-details">
								<h1 class="post-title"><?php echo $post['post_title']; ?></h1>
								<div class="post_author">Published by <a href="/core/profile/<?php echo $author; ?>"><?php echo $author; ?></a></div>
								<p class="recipe-description"> <?php echo $post['post_content']; ?></p>
								
									<!--<div class="post_date"><?php echo $postDate; ?></div>-->
									
								<div id="rating">
									<?php 
										$rating_value = 0;
										$ratings = $g->get_rating($post['post_id']);
										if(!empty($ratings['avg'])) {
											$rating_value = $ratings['avg'];
										?>
											<span>Rating <?php echo substr($rating_value, 0, 3); ?></span>
											<span>Total ratings <?php echo $ratings['amount']; ?></span>
									<?php 

										}
									?>

								<div class="rate-ex1-cnt">
									    <div id="1" class="rate-btn-1 rate-btn"></div>
									    <div id="2" class="rate-btn-2 rate-btn"></div>
									    <div id="3" class="rate-btn-3 rate-btn"></div>
									    <div id="4" class="rate-btn-4 rate-btn"></div>
									    <div id="5" class="rate-btn-5 rate-btn"></div>
									</div>
								</div>

								<div id="source">
									<h6>Recipe by</h6>
									<span class="source"><a href="<?php echo $post['post_source']; ?>">Source</a></span>
								</div>
							</div>
						</div>

							<div id="recipe_menu">
								<ul id="recipe_navigation">
									<li class="recipe-nutrition recipe-area">
										<span class="first">Calories</span>
										<span class="second"><?php echo $post['calories']; ?></span>
									</li>
									<li class="recipe-ingredients recipe-area">
										<span class="first">Time</span>
										<span class="second"><?php echo $post['time']; ?></span>
									</li>
									<li class="recipe-nutrition recipe-area">
										<span class="first">Serving</span>
										<span class="second"><?php echo $post['quantity']; ?></span>
									</li>
								</ul>
							</div>
						</div>

							<div class="play-area">
								<div class="helpful"> Start the timer to begin playing. </div>

								<div class="large-button light-blue timer">
									<a href="#timer"><img src="/core/images/timer.png" /> <span>Start Timer</span></a>
								</div>
								<span id="timer"></span>
							</div>		

							<div id="ingredients" class="recipe-section">
								<h4>Ingredients</h4>
								<?php echo $post['ingredients']; ?>
							</div>							

							<div id="method" class="recipe-section">
								<h4>Method</h4>
								<?php echo $post['method']; ?>
							</div>

							<div class="post_extras">
								<div class="extra">
									<h6> Difficulty </h6>
									<span class="difficulty"><?php echo $post['difficulty']; ?></span>
								</div>
								<div class="extra">
									<h6> Time </h6>
									<span class="time"><?php echo $post['time']; ?> minutes</span>
								</div>
								<div class="extra">
									<h6> Serving </h6>
									<span class="quantity"><?php echo $post['quantity']; ?> people</span>
								</div>
							</div>

							<?php if(isset($post['nutritional_info'])) { ?>
								<div id="nutritional_info" class="recipe-section">
									<h4>Nutritonal Information</h4>
									<p><?php echo $post['nutritional_info']; ?></p>
								</div>
							<?php } ?>
					
				<!--</div>-->

					<div id="recipe_comments">
					<h4 id="comment-header">Comments</h4>
						<section id="comments_list">
						<?php    
						    $get_comments = $g->get_comments($post['post_id']); 
						    //print_r($get_comments);

						    if(!empty($get_comments)) {

						    foreach($get_comments as $comment) {
						    ?>
						        <div class="comment">
						            <span class="comment_author"><a href="/core/profile/<?php echo $comment['username']; ?>"><?php echo $comment['username']; ?></a></span>
						            <p class="comment_content"><?php echo $comment['comment_post']; ?></p>
						        </div>
						    <?php
						            }
						        }
						        ?>
						</section>


						<h4> Leave some feedback? </h4>

						<form action="" method="post" id="form-comment" name="recipe_comment">

								<label for="comment_post"> Feedback </label>
								<span class="description">Tell them what you think of the recipe or if you would do something differently?</span>
								<textarea name="comment" id="comment" rows="6" tabindex="4"></textarea>

								<input id="post_comment" name="comment_submit" class="button post_comment" type="submit" onsubmit="return false" value="Post Comment" />
						</form>

						<?php 
						if($_POST['comment_submit'] == 'Post Comment') {
							//print_r($playerDetails);
							$comment = mysql_real_escape_string($_POST['comment']);
							$recipe_comment = $g->recipe_comment($playerDetails['userID'], $post['post_id'], $comment);
							//print_r($recipe_comment);
						}
						?>


						
					</div>
					

					<?php $experience = $post['post_experience']; ?>

					<div id="user_reward">
						<form method="post" class="reward" id="recipe-form">
							<input type="hidden" name="experience" value=<?php echo $experience; ?>>
							<input type="submit" id="recipe-finish" class="complete-recipe button" name="reward" value="Finished!" style="clear: both; float: left; padding: 25px 50px; background: seagreen; color: white; text-shadow: none;">
						</form>
						<?php 


							# post experience from invdividual post
							$post_exp = $_POST['experience'];

							//echo $playerDetails['userID'];

							//add current experience to post experience
							$exp_points = $playerDetails['experience'] + $post_exp;
		

							//if post has experience
							//earn experience points
							if(!empty($player_name)) {

								if(isset($_POST['reward'])) {
									# complete this recipe

									$finish = $g->complete_recipe($playerDetails['userID'], $post['post_id']);

									# check user achievements
									//if($check_achievement['status'] == 'active') {
					
									$achievement_check = $g->check_achievement($playerDetails['userID'], $post['category_id']);								

									$error = $g->get_errors();
									//print_r($error);

									
									//echo "checking" . print_r($achievement_check, true);
									
									
									if(!$error['0']) {
										# earn experience points
										$experience = $g->add_experience($player_name, $exp_points);
										//print_r($experience);

										//foreach ($achievement_check as $key => $value) {
											# code...
										//}
										$achievement = $g->action($playerDetails['username'], $achievement_check['ID'], 1);
										//print_r($achievement);

									# check what user is earning
									if(!empty($achievement)) {
										# player earns achievement
										echo "<section id='player-rewards'>";
										echo "<span id='close'></span>";
										
											
												echo "<div id='badge'><img class='achievement-badge' src='/core/achievements/" . $achievement['badge_src'] . "' /></div>";
												echo "<div id='player-achievement' class='achievement'>";
													echo "<h4> Congratulations, You have earned </h4>";
													echo "<h6 class='achievement-title'>" . $achievement['achievement_name'] . "</h6>";													
													echo "<span class='achievement-description'>" . $achievement['description'] . "</span>";
													echo "<div class='ach-exp-notification'><img class='exp-badge' src='/core/images/experience.png' /><span> " . $post_exp . " experience points </span></div>";
												echo "</div>";
									if($achievement_check['reward_title']) {
												echo "<div id='player-reward' class='reward'>";
													echo "<h5 class='title'>" . $achievement_check['reward_title'] . "</h5>";
													echo "<span class='code'>" . $achievement_check['code'] . "</span>";
													echo "<p class='reward-description'>" . $achievement_check['reward_description'] . "</p>";
												echo "</div>";
											}
										echo "</section>";
									}
		
										
									if(empty($achievement)) {
										echo "<div class='exp-notification'><img class='exp-badge' src='/core/images/experience-gold.png' /><span> " . $post_exp . " experience points </span></div>";
									}
										

										//echo 'achievement check' . print_r($check_achievement, true);

										# earn user achievement
										//$earn = $g->action($playerDetails['player_name'], );

									} else {
										echo '<div class="display_error"><span>'. $error['0'] .'<span></div>';
									}
								//}

									

									# after adding to user posts table check which posts completed call function to earn acheivement
									//print_r($finish);
								

								//echo 'Errors ' . print_r($error, true);

								//if post has acheievement

								//check user post achievements
								
								//earn achievements
								//$achievment = $g->action();
								//if(!$error['0']) {

										
									//}
								}
							}
							else if(isset($_POST['reward'])) {
								echo '<div class="display_error"><span>You need to be logged in to earn rewards.<span></div>';
							}
							
							//you have already completed this post.
						?>
							</div>
						</div>
					</div>
				</article>
			</div>
		</div>

    <script>
        // rating script - http://www.webcodo.net/rating-stars-system-with-jquery-ajax-php/#.Uz6DUK1dUfk
        $(function(){

        	for(i=0;i <= <?php echo $rating_value; ?>;i++) {
        		$('.rate-btn-'+i).addClass('rate-btn-hover');
        	}

            $('.rate-btn').hover(function(){
                $('.rate-btn').removeClass('rate-btn-hover');
                var therate = $(this).attr('id');
                for (var i = therate; i >= 0; i--) {
                    $('.rate-btn-'+i).addClass('rate-btn-hover');
                };
            });
                            
            $('.rate-btn').click(function(){    
                var therate = $(this).attr('id');
                var dataRate = 'act=rate&post_id=<?php echo $post['post_id']; ?>&user=<?php echo $playerDetails['userID']; ?>&rate='+therate; //
                $('.rate-btn').removeClass('rate-btn-active');
                for (var i = therate; i >= 0; i--) {
                    $('.rate-btn-'+i).addClass('rate-btn-active');
                };
                $.ajax({
                    type : "POST",
                    url : "/core/ajax_rating.php",
                    data: dataRate,
                    success:function(){}
                });
                
            });


              var comment_form = $('form#form-comment');
			  var comment_submit = $('#post_comment');

			  comment_form.on('submit', function(e) {
			    // prevent default action
			    e.preventDefault();

                var dataRate = $(this).serialize() + '&act=comment&user=<?php echo $playerDetails['userID']; ?>&post_id=<?php echo $post['post_id']; ?>'; //
                $.ajax({
                    type : "POST",
                    url : "/core/ajax_comment.php",
                    data: dataRate,
                    datatype: "html",
                    success:function(data){

                    	$('#comments_list').append(data).fadeIn('100');
                    }
                });
                
            });
        });

		$(document).ready(function() {
			var recipe_form = $('form#recipe-form');
			var recipe_submit = $('#recipe-finish');

				//close achievements popup
				$('span#close').click(function() {
					//console.log($(this).parent());
					$(this).parent('#player-rewards').slideToggle('slow');
				});

				$('.exp-notification').click(function() {
					//console.log($(this).parent());
					$(this).slideToggle('slow');
				});

			//recipe_form.on('submit', function(e) {
			//recipe_form.click(function(){    
                var dataRate = 'act=user-update&player=<?php echo $playerDetails['username']; ?>&experience=<?php echo $exp_points; ?>'; //
                $.ajax({
                    type : "POST",
                    url : "/core/header.php",
                    data: dataRate,
                    success:function(data){
                    	result = $(data).find(".level-bar-fill");
                    	result2 = $(data).find(".user_level");
                    	cssPercentage = $(result).children('span.percentage').text();
                    	levelID = $(result2).children('span#level-id').text();
                    	levelName = $(result2).children('span#level_name').text();

                    	$('.level-bar-fill').css('width', cssPercentage);
                    	$('.level-bar-fill span.percentage').text(cssPercentage);

                    	$('span#level-id').text(levelID);
                    	$('span#level_name').text(levelName);
                    }
                });

            /*
				Add to cart fly effect with jQuery. - May 05, 2013
				(c) 2013 @ElmahdiMahmoud - fikra-masri.by
				license: http://www.opensource.org/licenses/mit-license.php
			*/
                
            if($('.exp-notification').length > 0) {
           		var player = $('.level_bar');
		        //var imgtodrag = $(this).parent('.item').find("img").eq(0);
		        var imgtodrag = $('.exp-badge');
		        var player_area = $('.exp-notification');
		        if (imgtodrag) {
		            var imgclone = imgtodrag.clone()
		                .offset({
		                top: player_area.offset().top,
		                left: player_area.offset().left
		            })
		                .css({
		                'opacity': '0.5',
		                    'position': 'absolute',
		                    'height': '150px',
		                    'width': '150px',
		                    'z-index': '100'
		            })
		                .appendTo($('body'))
		                .animate({
		                'top': player.offset().top + 10,
		                    'left': player.offset().left + 10,
		                    'width': 75,
		                    'height': 75
		            }, 2000, 'easeInOutExpo');
		            
		            setTimeout(function () {
		                player.effect("shake", {
		                    times: 2
		                }, 200);
		            }, 1500);

		            imgclone.animate({
		                'width': 0,
		                    'height': 0
		            }, function () {
		                $(this).detach()
		            });
		        }
		    }
		});
    </script>

		<script type="text/javascript">
			//http://jsfiddle.net/r99Pa/
			$('#user_reward input.complete-recipe').hide();

		$('.large-button a').click(function() {
			var count = '<?php echo $post['time']; ?>:00';
			//count = count.replace('', ':00');

			$('#timer').html(count);

				var interval = setInterval(function() {
			    var timer = $('#timer').html();
			    timer = timer.split(':');
			    //timer = timer.split(':');
			    var minutes = parseInt(timer[0]);
			    var seconds = parseInt(timer[1]);
			    seconds -= 1;
			    if (minutes < 0) return clearInterval(interval);
			    if (minutes < 10 && minutes.length != 2) minutes = '0' + minutes;
			    if (seconds < 0 && minutes != 0) {
			        minutes -= 1;
			        seconds = 59;
			    }
			    else if (seconds < 10 && length.seconds != 2) seconds = '0' + seconds;
			    $('#timer').html(minutes + ':' + seconds);
			    
			    if (minutes == 0 && seconds == 0) {
			    	$('#timer').html('Your food is ready!');
			    	$('#user_reward input.complete-recipe').show();
			    	clearInterval(interval);
			    }

			}, 1000);
		});
		</script>
    
    <script>
        $('.exp-notification').flyto({
            item      : '.exp-notification',
            target    : '#playerActive',
            button    : '.exp-notification'
        });
    </script>
<?php include('footer.html'); ?>
