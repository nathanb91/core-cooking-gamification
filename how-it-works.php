<?php
	
	/*

	Author: Nathan Brettell
	Digital Media Project
	Gamification


	*/

	include('header.php');
?>
		<div id="main">
			<div id="main-background"></div>

			<div id="content">
				<article id="post">
					<h1 class="over-image"> How It Works </h1>

					<section class="information-area">
						<h2> Learn whilst you create </h2>
						<div class="image-placement">
						<!-- Image created by Nathan Brettell -->
							<img src="/core/images/learn-while-create.png">
						</div>
						<p> Increase your knowledge in a fun atmosphere through our host of recipes. The library includes favourite selections from a variety of sources, as well as recipes from you! </p>
					</section>

					<section class="information-area">
						<h2> Earn rewards, show off your creativity </h2>
						<div class="image-placement">
						<!-- Image created by Nathan Brettell -->
							<img src="/core/images/achievements-overview.png">
						</div>
						<p> It's not all about cooking. You have the opportunity to earn achievements and rewards based on the recipes you discover. These badges represent the skills you have learnt, and are comparable against other earners.</p>
					</section>
					
					<section class="information-area right">
						<h2> Work hard. Play hard. </h2>
						<div class="image-placement">
						<!-- Image created by Nathan Brettell -->
							<img src="/core/images/work-hard-play-hard.png">
						</div>
						<p> Look for new ways to increase your cooking skills and show them off to the people around you, looking for a new challenge? You've come to the right place! </p>
					</section>

				</article>
			</div>
		</div>

<?php include('footer.html'); ?>
