<?php
	
	/*

	Author: Nathan Brettell
	Digital Media Project
	Gamification

	Create your own recipe


	*/

	include('header.php');

	# check to make sure user exists

	$player_stats = $g->get_user($player);
	//$player_stats = true;

	//echo print_r($g->get_errors(), true);

	$error = $g->get_errors();
	//output pdo errors
	$g->debug();

	//echo '<pre>' . print_r($player_stats, true) . '</pre>';
	//print_r($error);


?>

		<div id="main">
		<div class="display_error"><span><?php if(isset($err)) echo $err['0']; ?></span></div>
			<div id="content">
				<article id="post">

				<h2>Have a recipe of your own?</h2>
					<form enctype="multipart/form-data" name="create_recipe" id="recipe-form" class="create-recipe" action="" method="post">	

						<input type="hidden" class="form-field" name="author" value="<?php echo $player['player_name']?>" />
						<!--<input type="hidden" class="form-field" name="slug" />-->
						<div class="segment-left">
							<span class="image-src">No file chosen</span>
							<img id="recipe-img" src="#" alt="test" />
							<div id="upload-area">
								<span class="upload-button">Upload</span>
								<input type="file" name="photo-field" id="photo-upload" class="form-upload" />	
							</div>

							<label for="description" class="form-label">Description: </label>
							<span class="field-description">Write a few sentences to describe your recipe.</span>
							<textarea name="description" class="form-field" id="description"></textarea>
						</div>
						
						<div class="segment-right">
							<label for="recipe-title" class="form-label">Name: </label>
							<span class="field-description">What is your recipe called?</span>
							<input type="text" id="recipe-title" name="recipe-title" class="form-field" maxlength="75" />

							<label for="recipe-slug" class="form-label">Slug: </label>
							<span class="field-description">The URL-friendly name</span>
							<input type="text" id="recipe-slug" name="recipe-slug" class="form-field" disabled="disabled" maxlength="75" />
							
							<?php $category_options = $g->get_categories(); ?>
							<label for="recipe-category" class="form-label">Category: </label>
							<span class="field-description">Pick a category</span>
							<select class="form-field" name="category">
								<?php foreach($category_options as $category) { ?>
									<option value="<?php echo $category['category_id']; ?>"><?php echo $category['category_name']; ?></option>
								<?php } ?>
							</select>

							<label for="directions" class="form-label">Directions: </label>
							<span class="field-description">What are the individual steps?</span>
							<textarea name="directions" class="form-field" id="directions"></textarea>

							<label for="ingredients" class="form-label">Ingredients: </label>
							<span class="field-description">People need to know what is needed to create your recipe.</span>
							<textarea name="ingredients" class="form-field" id="ingrds"></textarea>

							<label for="time" class="form-label">Time: </label>
							<span class="field-description">Please enter a number e.g. 20 for 20 minutes</span>
							<input type="text" class="form-field" name="time" />

							<label for="quantity" class="form-label">Quantity: </label>
							<span class="field-description">For how many people?</span>
							<input type="text" class="form-field" name="quantity" />
							
							<?php $difficulty_options = $g->get_difficulty(); ?>
							<label for="difficulty" class="form-label">Difficulty: </label>
							<span class="field-description">1 being fairly easy - 3 hard</span>
							<select class="form-field" name="difficulty">
								<?php foreach($difficulty_options as $difficulty) { ?>
									<option value="<?php echo $difficulty['difficulty_id']; ?>"><?php echo $difficulty['difficulty_name']; ?></option>
								<?php } ?>
							</select>

							<?php $level_options = $g->get_levels(); ?>
							<label for="level" class="form-label">Level: </label>
							<span class="field-description">What level do you need to be to unlock this recipe?</span>
							<select class="form-field" name="level">
								<?php foreach($level_options as $level) { ?>
									<option value="<?php echo $level['ID']; ?>"><?php echo $level['level_name']; ?></option>
								<?php } ?>
							</select>

							<label for="experience" class="form-label">Experience Points: </label>
							<span class="field-description">You can apply experience points to your post. </span>
							<input type="text" name="experience" class="form-field" id="exp" />

							<label for="nutrition" class="form-label">Nutritional Information: </label>
							<span class="field-description">How much; Fat? Sugar? etc</span>
							<textarea type="text" name="nutrition" class="form-field" id="nutrition"></textarea>

							<label for="calories" class="form-label">Calories: </label>
							<span class="field-description">How many calories are in this!</span>
							<input type="text" class="form-field" name="calories" />

							<label for="source" class="form-label">Source: </label>
							<span class="field-description">Own up to who's recipe this really is!</span>
							<input type="text" class="form-field" name="source" />
						</div>
							
						<input type="submit" class="button" name="submit" value="Create" />
					</form>
				<?php
					if($_POST['submit'] == 'Create') {

					$create_exp = 20;
				?>
					<div class="notification">
						<h1> Thank you for submitting a recipe.</h1>

							<p> Your recipe will be reviewed by administration before going live, please bear with us. You will recieve an email once your recipe has been approved.</p>

						<div class="large-button light-blue">
							<a href="/core/posts"><span>Continue Playing</span></a>
						</div>
					</div>

					<div class="exp-notification"><img class="exp-badge" src="/core/images/experience-gold.png" /><span><?php echo $create_exp; ?> experience points</span></div>

				<?php
					}
				?>
				</article>
			</div>
		</div>

		<?php //print_r($_POST); ?>
		<?php //print_r($_FILES); ?>
		<?php 
			//sanititzing user input
			$recipe_title = mysql_real_escape_string($_POST['recipe-title']);
			$slug = strtolower(str_replace(' ', '-', $recipe_title));
			$category = $_POST['category'];
			$recipe_description = nl2br($_POST['description']);
			$recipe_photo = mysql_real_escape_string($_POST['photo-field']);
			$post_date = date('Y-m-d');
			$recipe_level = $_POST['level'];
			$recipe_experience = mysql_real_escape_string($_POST['experience']);
			$recipe_source = mysql_real_escape_string($_POST['source']);
			$recipe_ingredients = nl2br($_POST['ingredients']);
			$recipe_directions = nl2br(mysql_real_escape_string($_POST['directions']));
			$recipe_nutrition = nl2br($_POST['nutrition']);
			$recipe_time = mysql_real_escape_string($_POST['time']);
			$recipe_difficulty = $_POST['difficulty'];
			$recipe_quantity = mysql_real_escape_string($_POST['quantity']);
			$recipe_calories = mysql_real_escape_string($_POST['calories']);

			$upload_directory = __DIR__ . '/images/recipes/';
			$recipe_image = '/core/images/recipes/' . $_FILES['photo-field']['name'];
			//echo $upload_directory;
			$upload_file = $upload_directory . $_FILES['photo-field']['name'];
			//echo $recipe_image;
			//upload the file to the upload directory
			move_uploaded_file($_FILES['photo-field']['tmp_name'], $upload_file);

		?>

		<?php

		//echo $player_name;

			if($_POST['submit'] == 'Create') {

				//add current experience to post experience
				$exp_points = $playerDetails['experience'] + $create_exp;

				# earn experience points
				$experience = $g->add_experience($player_name, $exp_points);

				$create_recipe = $g->create_recipe($player_name, $recipe_title, $slug, $category, $recipe_description, $recipe_image, $post_date, $recipe_experience, $recipe_level, $recipe_difficulty, $recipe_source, $recipe_ingredients, $recipe_directions, $recipe_nutrition, $recipe_time, $recipe_quantity, $recipe_calories); 
			}
		?>
		<script type="text/javascript">
		//help from http://jsfiddle.net/LvsYc/
		function readURL(input) {
	        if (input.files && input.files[0]) {
	            var reader = new FileReader();
	            
	            reader.onload = function (e) {
	                $('#recipe-img').attr('src', e.target.result);
	            }

	            	$('span.image-src').text(input.files[0].name);
	            	console.log(input.files);
	            
	            reader.readAsDataURL(input.files[0]);
	        }
	    }


	    
	    $("#photo-upload").change(function(){
	        readURL(this);
	    });

	    $('input#recipe-title').keyup(function() {
	    	//help to replace white-space to 'dashes' from http://stackoverflow.com/questions/1983648/replace-space-with-dash-and-make-all-letters-lower-case-using-javascript
	    	var input = $(this).val().replace(/\s+/g, '-').toLowerCase();
	    	console.log(input);
	    	$('input#recipe-slug').val(input);
	    });
		</script>

<?php include('footer.html'); ?>