jQuery(document).ready(function($) {
	//login box id
	var loginBox = $('.not-front #userForm');
	loginBox.addClass('hidden');
	loginBox.hide();

	function openLogin() {
		loginBox.slideToggle(600);
	}

	$('#openLogin').click(openLogin);

	//help from http://stackoverflow.com/questions/1403615/use-jquery-to-hide-a-div-when-the-user-clicks-outside-of-it
	$(document).mouseup(function (e)
		{
		    var container = $('#playerNotActive, #userForm');
		   

		    if (!container.is(e.target) // if the target of the click isn't the container...
		        && container.has(e.target).length === 0) // ... nor a descendant of the container
		    {
		    	//console.log('not container');
		    	loginBox.slideUp(600);
		    	loginBox.addClass('hidden');
		        //container.removeClass('hidden');
		    } 

		     //if (container.is(e.target)) {
		     	loginBox.removeClass('hidden');
		     //}
		    
	});

	/*$('.front #playerActive a#player').click(function() {
		$(this).next('ul#user-links').show().animate({right: '-110'});
	});

	$('.not-front #playerActive a#player').click(function() {
		$(this).next('ul#user-links').show().animate({right: '0'});
	});*/
	$('.not-front #playerActive a#player').click(function() {
		$(this).next('ul#user-links').slideToggle('200');
	});

	$('#nav ul li.nav-link').click(function() {
		$(this).children('ul.submenu').addClass('active').slideToggle('200');
		
	}), $('#content').click(function() {
			$('ul.submenu.active').slideUp('200');
	});


	$('nav a').each(function() {
		//console.log($(this).attr('href'));


		if(location.pathname == $(this).attr('href')) {
			$(this).addClass('active');
		}
	});

	
	var pathArray = window.location.pathname.split( '/' );
	$('body').addClass(pathArray[pathArray.length-1]);
	if(pathArray.length > 2) {
		$('body').addClass(pathArray[pathArray.length-2]);
	}
  


});