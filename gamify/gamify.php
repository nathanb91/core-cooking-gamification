<?php
/************************************************************* 
 * This script is developed by Arturs Sosins aka ar2rsawseen, http://webcodingeasy.com 
 * Feel free to distribute and modify code, but keep reference to its creator 
 * 
 * Gamify class allows to implement game logic into PHP aplications. 
 * It can create needed tables for storing information on most popular database platforms using PDO. 
 * It also can add users, define levels and achievements and generate user statistics and tops.
 * Then it is posible to bind class functions to user actions, to allow them gain experience and achievements.
 * 
 * For more information, examples and online documentation visit:  
 * http://webcodingeasy.com/PHP-classes/Implement-game-logic-to-your-web-application
**************************************************************/
class gamify
{
	private $con;
	private $pref = "gamify_";
	private $err = array();
	
	//create connection
	function __construct($host, $user, $pass, $db, $type = "mysql", $pref = "gamify_"){
		try{
			$this->con = new PDO($type.':host='.$host.';dbname='.$db, $user, $pass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
		} 
		catch(PDOException $e){
			$this->err[] = 'Error connecting to MySQL!: '.$e->getMessage();
		}
		$this->con->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT );
		$this->pref = $pref;
		//timezone not set fix
		date_default_timezone_set(date_default_timezone_get());
	}
	//install sql tables
	public function install(){
		//achievements
		$sql = "CREATE TABLE IF NOT EXISTS `".($this->pref)."achievements` (
		`ID` int(11) NOT NULL AUTO_INCREMENT,
		`achievement_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
		`badge_src` text COLLATE utf8_unicode_ci NOT NULL,
		`description` text COLLATE utf8_unicode_ci,
		`amount_needed` int(11) NOT NULL,
		`time_period` int(11) NOT NULL DEFAULT '0',
		`status` enum('active','inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
		PRIMARY KEY (`ID`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;";
		$this->con->exec($sql);

		//players
		$sql = "CREATE TABLE IF NOT EXISTS `".($this->pref)."users` (
		`userID` int(25) NOT NULL AUTO_INCREMENT,
		`username` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
		`password` text COLLATE utf8_unicode_ci NOT NULL,
		`email` text COLLATE utf8_unicode_ci,
		PRIMARY KEY (`userID`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;";
		$this->con->exec($sql);
		
		//levels
		$sql = "CREATE TABLE IF NOT EXISTS `".($this->pref)."levels` (
		`ID` int(11) NOT NULL AUTO_INCREMENT,
		`level_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
		`experience_needed` int(11) NOT NULL,
		PRIMARY KEY (`ID`)
		) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;";
		$this->con->exec($sql);
		
		try{
			//default zero level
			$sql = "INSERT INTO `".($this->pref)."levels` (`ID`, `level_name`, `experience_needed`) VALUES (1, '', 0);";
			$this->con->exec($sql);
		}
		catch(PDOException $e){
			$this->err[] = 'Error : '.$e->getMessage();
		}
		
		//user-achievement relation
		$sql = "CREATE TABLE IF NOT EXISTS `".($this->pref)."users_ach` (
		`ID` int(11) NOT NULL AUTO_INCREMENT,
		`userID` int(11) NOT NULL,
		`achID` int(11) NOT NULL,
		`amount` int(11) NOT NULL,
		`last_time` int(11) NOT NULL,
		`status` enum('active','completed') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
		PRIMARY KEY (`ID`),
		UNIQUE KEY `userID` (`userID`,`achID`),
		KEY `achID` (`achID`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;";
		$this->con->exec($sql);
		
		//users and statistics
		//change ID to userID with foreign key to gamify_users->userID
		$sql = "CREATE TABLE IF NOT EXISTS `".($this->pref)."user_stats` (
		`ID` int(11) NOT NULL AUTO_INCREMENT,
		`username` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
		`experience` int(11) NOT NULL DEFAULT '0',
		`level` int(11) NOT NULL DEFAULT '1',
		PRIMARY KEY (`ID`),
		UNIQUE KEY `username` (`username`),
		KEY `level` (`level`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;";
		$this->con->exec($sql);
		
		try{
			//user-achievement relation constraints
			$sql = "ALTER TABLE `".($this->pref)."users_ach`
			ADD CONSTRAINT `".($this->pref)."users_ach_ibfk_2` FOREIGN KEY (`achID`) REFERENCES `".($this->pref)."achievements` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
			ADD CONSTRAINT `".($this->pref)."users_ach_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `".($this->pref)."user_stats` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;";
			$this->con->exec($sql);
			
			//user level constraint
			$sql = "ALTER TABLE `".($this->pref)."user_stats`
			ADD CONSTRAINT `".($this->pref)."user_stats_ibfk_1` FOREIGN KEY (`level`) REFERENCES `".($this->pref)."levels` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE;";
			$this->con->exec($sql);
		}
		catch(PDOException $e){
			$this->err[] = 'Error : '.$e->getMessage();
		}
	}
	//output error messages
	public function debug(){
		$this->con->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
	}
	
	//get internal error array
	public function get_errors(){
		return $this->err;
	}
	
	/**************************
	* USER MANIPULATIONS
	**************************/
	//create user
	//extend to create user registration/login -NB
	//create userID relation from gamify_users table to gamify_user_stats - NB
	public function create_user($username, $password, $email){ //$username, $password, $email (& dates)
		try{
			//$query = $this->con->prepare("SELECT ID FROM `".($this->pref)."user_stats` WHERE `username` = ?");
			$query = $this->con->prepare("SELECT userID FROM `".($this->pref)."users` WHERE `username` = ?");
			$query->execute(array($username));
			//check if user doesn't exist
			if(!$query->fetch())
			{
				$query->closeCursor();
				$user = true;
				$date = date('Y-m-d');
				//$query = $this->con->prepare("INSERT INTO `".($this->pref)."user_stats` SET `username` = ?");
				$query = $this->con->prepare("INSERT INTO `".($this->pref)."users` SET `username` = ?, `password` = ?, `email` = ?, `join_date` = '$date'");
				$query->execute(array($username, $password, $email));
				$query->closeCursor();
			}
			if($user) {
				//create userID relation to user_stats table
					
					$query = $this->con->prepare("SELECT userID, username FROM `".($this->pref)."users` WHERE `username` = ?");
					$query->execute(array($username));

					//$person = $query->fetch();
					//$query->closeCursor();
					//return $person;

			if($person = $query->fetch()) {
						
				$query = $this->con->prepare("INSERT INTO `".($this->pref)."user_stats` SET `playername` = ?, `userID` = ?");
				$query->execute(array($person["username"], $person["userID"]));
				$query->closeCursor();
				return $person;
				}
			}
			if(!$user) {
				$this->err[] = 'This user already exists, please choose a different username';
			}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error : '.$e->getMessage();
		}
	}
	//user login - created NB
	public function user_login($username, $password) {
		try{
			$query = $this->con->prepare("SELECT userID, username, email FROM `".($this->pref)."users` WHERE `username` = ? AND password = ?");
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query->execute(array($username, $password));
			//$query->closeCursor();
			if($player = $query->fetch())
			{
				$query->closeCursor();
				//$query = $this->con->prepare("INSERT INTO `".($this->pref)."user_stats` SET `username` = ?");
				$query = $this->con->prepare("SELECT * FROM `".($this->pref)."users` WHERE `username` = ".$username." AND `password` = ".$password."");
				$query->setFetchMode(PDO::FETCH_ASSOC);

				//check below code (could be wrong / not working as intended)
				$query->execute();
				//$player["userID"] = $query->fetch();
				$query->closeCursor();
				return $player;
			}
			else
			{
				$this->err[] = 'Sorry, please check your details';
			}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error : '.$e->getMessage();
		}
	}
	//get one user information
	public function get_user($username){
		try{
			//$query = $this->con->prepare("SELECT a.ID, a.username, a.experience, b.level_name as level FROM `".($this->pref)."user_stats` as a,`".($this->pref)."levels` as b WHERE a.level = b.ID and `username` = ?");
			//$query = $this->con->prepare("SELECT player.username, player.userID, player_stats.experience, level.level_name as level FROM `".($this->pref)."users` as player,`".($this->pref)."user_stats` as player_stats,`".($this->pref)."levels` as level WHERE player_stats.level = level.ID and player.username = ?");
			
			//improved query to use join levels and stats - NB	
			$query = $this->con->prepare("SELECT player.avatar_source, player.username, player.userID, player_stats.experience, player_stats.level, levels.level_name FROM `".($this->pref)."users` as player INNER JOIN `".($this->pref)."user_stats` as player_stats ON player.userID = player_stats.userID 
											INNER JOIN `".($this->pref)."levels` as levels ON player_stats.level = levels.ID WHERE player.username = ?");

			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query->execute(array($username));
			if($result = $query->fetch())
			{

				$query->closeCursor();
				//$query = $this->con->prepare("SELECT b.achievement_name, b.badge_src, a.amount as amount_got, b.amount_needed, a.last_time as time, a.status FROM `".($this->pref)."users_ach` as a, `".($this->pref)."achievements` as b WHERE a.achID = b.ID and a.userID = ?");				
				$query = $this->con->prepare("SELECT achID, ach.achievement_name, ach.badge_src, u_ach.amount as amount_got, ach.amount_needed, u_ach.last_time as time, u_ach.status FROM `".($this->pref)."users_ach` as u_ach, `".($this->pref)."achievements` as ach WHERE u_ach.achID = ach.ID and u_ach.userID = ?");
				$query->setFetchMode(PDO::FETCH_ASSOC);
				$query->execute(array($result['userID']));
				$result["achievements"] = $query->fetchAll();
				$query->closeCursor();
				return $result;
			}
			else
			{
				$this->err[] = 'There is no user with username '. $username;
			}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error : '.$e->getMessage();
		}
	}
	//upload avatar - NB
	public function upload_avatar($userID, $avatar){
		try{	
			$query = $this->con->prepare("UPDATE `".($this->pref)."users` SET avatar_source = ? WHERE userID = ?");

			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query->execute(array($avatar, $userID));
			if($result = $query->fetch())
			{

				$query->closeCursor();
				return $result;
			}
			else
			{
				$this->err[] = 'Upload failed'. $username;
			}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error : '.$e->getMessage();
		}
	}

	//get multiple user information
	public function get_users($ord = "", $desc = false, $limit = 0){
		$add = "";
		if(in_array($ord, array("username", "experience")))
		{
			$add .= " ORDER BY ".$ord;
			if($desc)
			{
				$add .= " DESC";
			}
		}
		$limit = intval($limit);
		if($limit > 0)
		{
			$add .= " LIMIT ".$limit;
		}
		try{
			//$query = $this->con->prepare("SELECT a.ID, a.username, a.experience, b.level_name as level FROM `".($this->pref)."user_stats` as a,`".($this->pref)."levels` as b WHERE a.level = b.ID".$add);
			$query = $this->con->prepare("SELECT player_stats.ID, player_stats.playername, player_stats.experience, player_level.level_name as level FROM `".($this->pref)."user_stats` as player_stats,`".($this->pref)."levels` as player_level WHERE player_stats.level = player_level.ID".$add);
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query->execute();
			if($result = $query->fetchAll())
			{
				$query->closeCursor();
				return $result;
			}
			else
			{
				$this->err[] = 'There are no users';
			}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error : '.$e->getMessage();
		}
	}
	public function leaderboard($ord = "", $desc = false, $limit = 0){
		$add = "";
		if(in_array($ord, array("username", "experience")))
		{
			$add .= " ORDER BY ".$ord;
			if($desc)
			{
				$add .= " DESC";
			}
		}
		$limit = intval($limit);
		if($limit > 0)
		{
			$add .= " LIMIT ".$limit;
		}
		try{
			//$query = $this->con->prepare("SELECT a.ID, a.username, a.experience, b.level_name as level FROM `".($this->pref)."user_stats` as a,`".($this->pref)."levels` as b WHERE a.level = b.ID".$add);
			$query = $this->con->prepare("SELECT player_stats.ID, player_stats.playername, player_stats.experience, player_level.level_name as level FROM `".($this->pref)."user_stats` as player_stats,`".($this->pref)."levels` as player_level WHERE player_stats.level = player_level.ID AND player_stats.last_updated > DATE_ADD(CURRENT_TIMESTAMP, INTERVAL -1 MONTH) ORDER BY player_stats.experience DESC LIMIT 0, 10");
			//print_r($query);
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query->execute();
			if($result = $query->fetchAll())
			{
				$query->closeCursor();
				return $result;
			}
			else
			{
				$this->err[] = 'There are no users';
			}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error : '.$e->getMessage();
		}
	}

	//get a related user leaderboard for ranking - NB
	public function user_leaderboard($one, $two, $three) {
	try{
		//http://stackoverflow.com/questions/3672283/how-can-i-select-adjacent-rows-to-an-arbitrary-row-in-sql-or-postgresql
		//$query = $this->con->prepare("SELECT a.ID, a.username, a.experience, b.level_name as level FROM `".($this->pref)."user_stats` as a,`".($this->pref)."levels` as b WHERE a.level = b.ID".$add);
		$query = $this->con->prepare("(SELECT * FROM `".($this->pref)."user_stats` WHERE userID >= ? ORDER BY experience DESC LIMIT 4) UNION (SELECT * FROM `".($this->pref)."user_stats` WHERE userID < ? ORDER BY experience DESC LIMIT 3) UNION (SELECT * FROM `".($this->pref)."user_stats` WHERE userID = ?) ORDER BY experience DESC");
		//print_r($query);
		$query->setFetchMode(PDO::FETCH_ASSOC);
		$query->execute(array($one, $two, $three));
		if($result = $query->fetchAll())
		{
			$query->closeCursor();
			return $result;
		}
		else
		{
			$this->err[] = 'There are no users';
			}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error : '.$e->getMessage();
		}
	}

	//ajax search for the main homepage - NB
	public function ajax_search() {
		try{

			$query = $this->con->prepare("SELECT post_title, post_slug FROM `".($this->pref)."posts` WHERE `post_title` LIKE :term");
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query->execute(array('term' => '%'.$_GET['term'].'%'));
			//$query->closeCursor();

			$return_arr = array();
			$json_array = array();

			while($row = $query->fetch()) {

				$return_arr['value'] = $row['post_title'];
				$return_arr['id'] = $row['post_slug'];
			    //$return_arr = array('title' => $row['post_title'], 'url' => $row['post_slug']);
			    array_push($json_array, $return_arr); 
        	}

        	//print_r($json_array);
			
		}
			catch(PDOException $e) {
			$this->err[] = 'Error : '.$e->getMessage();
		}
			
			echo json_encode($json_array);
	}
	//recipe search function in the template header - NB
	public function recipe_search() {
		try{

			$query = $this->con->prepare("SELECT * FROM `".($this->pref)."posts` WHERE `post_content` LIKE :search");
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query->execute(array('search' => '%'.$_POST['search-form'].'%'));
			//$query->closeCursor();

			while($row = $query->fetchAll()) {
				$query->closeCursor();
				return $row;
        	}
			
		}
			catch(PDOException $e) {
			$this->err[] = 'Error : '.$e->getMessage();
		}
	}
	//edit users info
	public function edit_user($id, $username = "", $experience = "", $level = ""){
		try{
			$query = $this->con->prepare("SELECT * FROM `".($this->pref)."user_stats` WHERE `ID` = ?");
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query->execute(array($id));
			if($user = $query->fetch())
			{
				$query->closeCursor();
				$username = ($username == "") ? $user["username"] : $username;
				$experience = ($experience == "") ? $user["experience"] : $experience;
				$level = ($level == "") ? $user["level"] : $level;
				$query = $this->con->prepare("UPDATE `".($this->pref)."user_stats` SET `username` = ?, `experience` = ?, `level` = ? WHERE `ID` = ?");
				$query->execute(array($username, $experience, $level, $id));
				$query->closeCursor();
			}
			else
			{
				$this->err[] = 'User with ID '.$id.' does not exist';
			}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error : '.$e->getMessage();
		}
	}
	//delete user
	public function delete_user($username){
		try{
			$query = $this->con->prepare("DELETE FROM `".($this->pref)."user_stats` WHERE `username` = ?");
			$query->execute(array($username));
			$query->closeCursor();
		}
		catch(PDOException $e) {
			$this->err[] = 'Error : '.$e->getMessage();
		}
	}
	
	/**************************
	* LEVEL MANIPULATIONS
	**************************/
	//create new level
	public function create_level($name, $exp){
		try{
			$query = $this->con->prepare("INSERT INTO `".($this->pref)."levels` SET `level_name` = ?, `experience_needed` = ?");
			$query->execute(array($name, $exp));
			$query->closeCursor();
		}
		catch(PDOException $e) {
			$this->err[] = 'Error : '.$e->getMessage();
		}
	}
	//get level
	public function get_level($id){
		try{
			$query = $this->con->prepare("SELECT * FROM `".($this->pref)."levels` WHERE `ID` = ?");
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query->execute(array($id));
			if($result = $query->fetch())
			{
				$query->closeCursor();
				return $result;
			}
			else
			{
				$this->err[] = 'There are no level with ID '.$id;
			}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error : '.$e->getMessage();
		}
	}
	// get the next level
	public function get_next_level($id){
		try{
			$query = $this->con->prepare("SELECT * FROM `".($this->pref)."levels` WHERE `ID` > ? LIMIT 1");
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query->execute(array($id));
			if($result = $query->fetch())
			{
				$query->closeCursor();
				return $result;
			}
			else
			{
				$this->err[] = 'There are no level with ID '.$id;
			}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error : '.$e->getMessage();
		}
	}
	//get levels
	public function get_levels($ord = "", $desc = false, $limit = 0){
		$add = "";
		if(in_array($ord, array("level_name", "experience_needed")))
		{
			$add .= " ORDER BY ".$ord;
			if($desc)
			{
				$add .= " DESC";
			}
		}
		$limit = intval($limit);
		if($limit > 0)
		{
			$add .= " LIMIT ".$limit;
		}
		try{
			$query = $this->con->prepare("SELECT * FROM `".($this->pref)."levels`".$add);
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query->execute();
			if($result = $query->fetchAll())
			{
				$query->closeCursor();
				return $result;
			}
			else
			{
				$this->err[] = 'There are no levels';
			}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error : '.$e->getMessage();
		}
	}
	//edit level
	public function edit_level($id, $name = "", $experience = ""){
		try{
			$query = $this->con->prepare("SELECT * FROM `".($this->pref)."levels` WHERE `ID` = ?");
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query->execute(array($id));
			if($level = $query->fetch())
			{
				$query->closeCursor();
				$name = ($name == "") ? $level["level_name"] : $name;
				$experience = ($experience == "") ? $level["experience_needed"] : $experience;
				$query = $this->con->prepare("UPDATE `".($this->pref)."levels` SET `level_name` = ?, `experience_needed` = ? WHERE `ID` = ?");
				$query->execute(array($name, $experience, $id));
				$query->closeCursor();
			}
			else
			{
				$this->err[] = 'There are no level with ID '.$id;
			}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error : '.$e->getMessage();
		}
	}
	//delete level
	public function delete_level($id){
		if($id != 1)
		{
			try{
				$query = $this->con->prepare("DELETE FROM `".($this->pref)."user_stats` WHERE `ID` = ?");
				$query->execute(array($id));
				$query->closeCursor();
				$query = $this->con->prepare("UPDATE `".($this->pref)."user_stats` SET `level` = '1' WHERE `level` = ?");
				$query->execute(array($id));
				$query->closeCursor();
			}
			catch(PDOException $e) {
				$this->err[] = 'Error : '.$e->getMessage();
			}
		}
		else
		{
			$this->err[] = 'Can\'t delete default level. It can only be edited';
		}
	}
	
	/**************************
	* ACHIEVEMENT MANIPULATIONS
	**************************/
	//create new achievement
	public function create_achievement($name, $amount, $period = 0, $badge = "", $description = ""){
		try{
			$query = $this->con->prepare("INSERT INTO `".($this->pref)."achievements` SET `achievement_name` = ?, `amount_needed` = ?, `time_period` = ?, `badge_src` = ?, `description` = ?");
			$query->execute(array($name, $amount, $period, $badge, $description));
			$query->closeCursor();
		}
		catch(PDOException $e) {
			$this->err[] = 'Error : '.$e->getMessage();
		}
			
	}
	//get achievement
	public function get_achievement($id){
		try{
			$query = $this->con->prepare("SELECT * FROM `".($this->pref)."achievements` WHERE `ID` = ?");
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query->execute(array($id));
			if($result = $query->fetch())
			{
				$query->closeCursor();
				return $result;
			}
			else
			{
				$this->err[] = 'There are no achievements with ID '.$id;
			}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error : '.$e->getMessage();
		}
	}
	//get achievements
	public function get_achievements($ord = "", $desc = false, $limit = 0){
		$add = "";
		if(in_array($ord, array("achievement_name", "amount_needed")))
		{
			$add .= " ORDER BY ".$ord;
			if($desc)
			{
				$add .= " DESC";
			}
		}
		$limit = intval($limit);
		if($limit > 0)
		{
			$add .= " LIMIT ".$limit;
		}
		try{
			$query = $this->con->prepare("SELECT * FROM `".($this->pref)."achievements`".$add);
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query->execute();
			if($result = $query->fetchAll())
			{
				$query->closeCursor();
				return $result;
			}
			else
			{
				$this->err[] = 'There are no achievements';
			}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error : '.$e->getMessage();
		}
	}
	//edit achievement
	public function edit_achievement($id, $name = "", $amount = "", $period = "", $badge = "", $description = "", $status = "active"){
		try{
			$query = $this->con->prepare("SELECT * FROM `".($this->pref)."achievements` WHERE `ID` = ?");
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query->execute(array($id));
			if($ach = $query->fetch())
			{
				$query->closeCursor();
				$name = ($name == "") ? $ach["achievement_name"] : $name;
				$badge = ($badge == "") ? $ach["badge_src"] : $badge;
				$description = ($description == "") ? $ach["description"] : $description;
				$amount = ($amount == "") ? $ach["amount_needed"] : $amount;
				$period = ($period == "") ? $ach["time_period"] : $period;
				$query = $this->con->prepare("UPDATE `".($this->pref)."achievements` SET `achievement_name` = ?, `badge_src` = ?, `amount_needed` = ?, `time_period` = ?, `status` = ? WHERE `ID` = ?");
				$query->execute(array($name, $badge, $amount, $period, $status, $id));
				$query->closeCursor();
			}
			else
			{
				$this->err[] = 'There are no achievements with ID '.$id;
			}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error : '.$e->getMessage();
		}
	}
	//delete achievement
	public function delete_achievement($id){
		try{
			$query = $this->con->prepare("DELETE FROM `".($this->pref)."achievements` WHERE `ID` = ?");
			$query->execute(array($id));
			$query->closeCursor();
		}
		catch(PDOException $e) {
			$this->err[] = 'Error : '.$e->getMessage();
		}
	}
	//disable achievement
	public function disable_achievement($id){
		$this->edit_achievement($id, "", "", "", "", "", "inactive");
	}
	//enable achievement
	public function enable_achievement($id){
		$this->edit_achievement($id, "", "", "", "", "", "active");
	}
	
	/**************************
	* POST DIRECTORY
	**************************/
	//get recipe information
	public function get_posts($levelID) {
		try{
			$query = $this->con->prepare("SELECT * FROM `".($this->pref)."posts` WHERE `".($this->pref)."posts`.post_level <= ? ORDER BY RAND()");
			$query->setFetchMode(PDO::FETCH_ASSOC);
			//$query->execute(array($id));
			$query->execute(array($levelID));
			if($posts = $query->fetchAll()) {
				$query->closeCursor;
				return $posts;
			}
			else 
			{
				//throw an error
				$this->err[] = 'Error post unavailable';
			}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error'.$e->getMessage();
		}
	}
	//function to get posts based on rating greater than 3.5 - NB
	public function get_popular_posts($levelID) {
		try{
			//query help from - http://stackoverflow.com/questions/9423748/sql-query-how-to-get-item-with-highest-rating
			$query = $this->con->prepare("SELECT * FROM `gamify_posts` posts JOIN ( SELECT post_id, rating_id, AVG( rating ) AS avg FROM `gamify_post_rating` GROUP BY post_id ) `gamify_post_rating` ON `posts`.post_id = `gamify_post_rating`.post_id WHERE `posts`.post_level <= ? AND `gamify_post_rating`.avg >= 3.5 ORDER BY RAND()");
			$query->setFetchMode(PDO::FETCH_ASSOC);
			//$query->execute(array($id));
			$query->execute(array($levelID));
			if($posts = $query->fetchAll()) {
				$query->closeCursor;
				return $posts;
			}
			else 
			{
				//throw an error
				$this->err[] = 'Error post unavailable';
			}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error'.$e->getMessage();
		}
	}
	//function to get posts based on time - NB
	public function get_quick_simple_posts($levelID) {
		try{
			$query = $this->con->prepare("SELECT * FROM `".($this->pref)."posts` INNER JOIN `".($this->pref)."post_schema` ON `".($this->pref)."posts`.post_id = `".($this->pref)."post_schema`.post_id WHERE `".($this->pref)."posts`.post_level <= ? AND `".($this->pref)."post_schema`.time < 20");
			$query->setFetchMode(PDO::FETCH_ASSOC);
			//$query->execute(array($id));
			$query->execute(array($levelID));
			if($posts = $query->fetchAll()) {
				$query->closeCursor;
				return $posts;
			}
			else 
			{
				//throw an error
				$this->err[] = 'Error post unavailable';
			}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error'.$e->getMessage();
		}
	}
	//function to get filter the posts used by AJAX - NB
	public function filter_posts($levelID, $difficulty) {
		try{
			if($levelID >= 0) {
				$query = $this->con->prepare("SELECT * FROM `".($this->pref)."posts` WHERE `".($this->pref)."posts`.post_level = ?");
				$query->execute(array($levelID));
				$query->setFetchMode(PDO::FETCH_ASSOC);
			}
			if($difficulty) {
				$query = $this->con->prepare("SELECT * FROM `".($this->pref)."posts` WHERE `".($this->pref)."posts`.difficulty = ?");
				$query->execute(array($difficulty));
				$query->setFetchMode(PDO::FETCH_ASSOC);
			}
			
			//$query->execute(array($id));
			
			if($posts = $query->fetchAll()) {
				$query->closeCursor;
				return $posts;
			}
			else 
			{
				//throw an error
				$this->err[] = 'Error post unavailable';
			}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error'.$e->getMessage();
		}
	}
	//view individual recipe - NB
	public function view_post($slug) {
		try {
			$query = $this->con->prepare("SELECT * FROM `gamify_posts` LEFT JOIN `gamify_posts_directory` ON `gamify_posts_directory`.post_id = `gamify_posts`.post_id LEFT JOIN  `gamify_post_schema` ON  `gamify_post_schema`.schema_id =  `gamify_posts_directory`.schema_id WHERE `gamify_posts`.`post_slug` = ?");
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query->execute(array($slug));

			if($result = $query->fetch()) {
				
				$query->closeCursor;
				return $result;
				
			}
			else 
			{
				//throw an error
				$this->err[] = 'There are no posts';
			}
		}
		catch(PDOException $e) {
			$this->error = 'Error no posts'.$e->getMessage();
		}
	}
	//function to get the recipe category - NB
	public function post_category($post_id) {
		try {
			$query = $this->con->prepare("SELECT post_category.category_id, category_slug, category_name FROM `".($this->pref)."posts_category` as post_category, `".($this->pref)."posts_directory` as post_directory WHERE post_category.category_id = post_directory.category_id AND post_directory.post_id = ? ");
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query->execute(array($post_id));

			if($categories = $query->fetchAll()) {
				$query->closeCursor();
				return $categories;
			}
			else 
			{
			//throw an error
			$this->err[] = 'There are no posts';
			}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error no posts'.$e->getMessage();
		}
	}
	//function to get a single category for category template - NB
	public function get_category($cat_slug) {
		$query = $this->con->prepare("SELECT category_id, category_name, category_slug FROM `".($this->pref)."posts_category` WHERE category_slug = ?");
		$query->setFetchMode(PDO::FETCH_ASSOC);
		$query->execute(array($cat_slug));

		if($category = $query->fetch()) {
			$query->closeCursor();
			return $category;
		}
	}
	//get list of all categories for menu - NB
	public function get_categories() {
		$query = $this->con->prepare("SELECT * FROM `".($this->pref)."posts_category`");
		$query->setFetchMode(PDO::FETCH_ASSOC);
		$query->execute();

		if($category = $query->fetchAll()) {
			$query->closeCursor();
			return $category;
		}
	}
	//view posts overview based on their category - NB
	public function category_posts($cat, $level) {
		try {
			$query = $this->con->prepare("SELECT * FROM `".($this->pref)."posts` as post, `".($this->pref)."posts_directory` as post_directory WHERE post.post_id = post_directory.post_id AND post_directory.category_id = ? AND post.post_level <= ? ORDER BY post.post_id DESC");
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query->execute(array($cat, $level));

			if($result = $query->fetchAll()) {
				$query->closeCursor;
				return $result;
			}
		}
		catch(PDOException $e) {
			$this->error= 'Error no posts'.$e->getMessage();
		}
	}

	//get list of all difficulties for menu - NB
	public function get_difficulty() {
		$query = $this->con->prepare("SELECT * FROM `".($this->pref)."post_difficulty`");
		$query->setFetchMode(PDO::FETCH_ASSOC);
		$query->execute();

		if($difficulty = $query->fetchAll()) {
			$query->closeCursor();
			return $difficulty;
		}
	}

	//complete a recipe marking as complete - NB
	public function complete_recipe($userID, $postID) {
		try {
			$query = $this->con->prepare("SELECT userID, post_id FROM `".($this->pref)."user_posts` WHERE `userID` = ? AND `post_id` = ?");
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query->execute(array($userID, $postID));

				# if user hasn't completed this post
				if(!$query->fetch()) {
					
					//$query->closeCursor();
					//insert into gamify_user_posts table
					//$query = $this->con->prepare("INSERT INTO `".($this->pref)."user_posts` SET `userID` = ? AND `post_id` = ?");
										//insert into gamify_user_posts table
					$query = $this->con->prepare("INSERT INTO `".($this->pref)."user_posts` (`userID`, `post_id`) VALUES (?, ?)");
					//echo print_r($result, true);

					$query->execute(array($userID, $postID));
					//$query->execute(array($result['userID'], $result['post_id']));
					$query->closeCursor();
				}
				else
				{
					# user has complete the post
					$this->err[] = 'You have already completed this recipe.';
				}
			//}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error '.$e->getMessage();
		}
	}

	//recipe comment function - NB
	public function recipe_comment($userID, $postID, $post_comment) {
		try {
			$query = $this->con->prepare("SELECT userID, post_id FROM `".($this->pref)."user_posts` WHERE `userID` = ? AND `post_id` = ?");
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query->execute(array($userID, $postID));

				# if user has posted a comment
				if($comments = $query->fetch()) {
					//print_r($comments);
					$query->closeCursor();
					$query = $this->con->prepare("INSERT INTO `".($this->pref)."post_comments` (`comment_author`, `post_id`, `comment_post`) VALUES (?, ?, ?)");
					//echo print_r($result, true);

					$query->execute(array($userID, $postID, $post_comment));
					//$query->execute(array($result['userID'], $result['post_id']));
					return $comments;
				}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error '.$e->getMessage();
		}
	}
	//function to retrieve all comments for the post - NB
	public function get_comments($postID) {
		try {
			$query = $this->con->prepare("SELECT * FROM `".($this->pref)."post_comments` INNER JOIN `".($this->pref)."users` users ON users.userID = `".($this->pref)."post_comments`.comment_author WHERE `post_id` = ?");
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query->execute(array($postID));
			
			if($comments = $query->fetchAll()) {
				$query->closeCursor();
				return $comments;
			}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error '.$e->getMessage();
		}
	}
	//recipe rating function - NB
	public function recipe_rating($userID, $postID, $rating) {
		try {
			$query = $this->con->prepare("SELECT userID, post_id FROM `".($this->pref)."user_posts` WHERE `userID` = ? AND `post_id` = ?");
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query->execute(array($userID, $postID));

				# if user has posted a comment
				if($comments = $query->fetchAll()) {
					//print_r($comments);
					$query->closeCursor();
					$query = $this->con->prepare("INSERT INTO `".($this->pref)."post_rating` (`user_id`, `post_id`, `rating`) VALUES (?, ?, ?)");
					//echo print_r($result, true);

					$query->execute(array($userID, $postID, $rating));
					//$query->execute(array($result['userID'], $result['post_id']));
					return $comments;
				}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error '.$e->getMessage();
		}
	}
	//function to get the current recipe rating - NB
	public function get_rating($postID) {
		try {
			$query = $this->con->prepare("SELECT count(post_id) as amount, post_id, rating_id, AVG( rating ) AS avg FROM `gamify_post_rating` WHERE post_id = ? GROUP BY post_id");
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query->execute(array($postID));
			
			if($ratings = $query->fetch()) {
				$query->closeCursor();
				return $ratings;
			}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error '.$e->getMessage();
		}
	}

	/**************************
	* USER INTERACTION
	**************************/
	//add experience to user
	public function add_experience($username, $exp){
		try{
			//get info
			//$query = $this->con->prepare("SELECT * FROM `".($this->pref)."user_stats` WHERE `username` = ?");
			$query = $this->con->prepare("SELECT * FROM `".($this->pref)."user_stats` WHERE `playername` = ?");		
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query->execute(array($username));
			//get current date
			$date = date('Y-m-d G-i-s');
			if($row = $query->fetch())
			{
				$query->closeCursor();
				//$exp += $row["experience"];
				//check if new level
				$query = $this->con->prepare("SELECT * FROM `".($this->pref)."levels` WHERE `experience_needed` = (SELECT max(`experience_needed`) FROM `".($this->pref)."levels` WHERE `experience_needed` <= ? AND `".($this->pref)."levels`.ID > ?)");
				$query->execute(array($exp, $row['level']));
				$query->setFetchMode(PDO::FETCH_ASSOC);
				$level = $query->fetch();
				if($level && $level["ID"] != $row["level"])
				{
					$query->closeCursor();
					//update experience and level info
					//print_r($row);
					//print_r($level);

					$new_level = $row['experience'] + $exp;

					if($new_level > $level['experience_needed']) {
						$post_exp = (int)$exp - (int)$row['experience'];

						//225 - 215 = 10
						//210 + 45 = 260 - 225 = 35


						$exp = $row['experience'] + $post_exp;
						$exp = $exp - $level['experience_needed'];
					}

					//$level['experience_needed'] - $row['experience']

					// ID to userID
					//match experience against experience needed - revert to 0 if user has gained a level
					$query = $this->con->prepare("UPDATE `".($this->pref)."user_stats` SET `experience` = ?, `level` = ?, `last_updated` = '$date' WHERE `userID` = ?");
					$query->execute(array($exp, $level["ID"], $row["userID"]));
					$query->closeCursor();
					return $level;
				}
				else
				{
					$query->closeCursor();
					//update experience info

					// ID to userID
					$query = $this->con->prepare("UPDATE `".($this->pref)."user_stats` SET `experience` = ?, `last_updated` = '$date' WHERE `userID` = ?");
					$query->execute(array($exp, $row["userID"]));
					$query->closeCursor();
				}
			}
			else
			{	
				//user doesn't exist, create user and add experience
				$this->create_user($username, $password, $email);
				$this->add_experience($username, $exp);
			}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error : '.$e->getMessage();
		}
		return false;
	}

	//check to see if user has earned an achievement - NB
	public function check_achievement($player, $category){ //$player, $postID, $category, $level
		try {
			//Achievement: First completed recipe
			if($player) {
				$query = $this->con->prepare("SELECT count(userID) FROM `".($this->pref)."user_posts` WHERE userID = ?");
				$query->setFetchMode(PDO::FETCH_ASSOC);
				$query->execute(array($player));

					if($result = $query->fetch())
					{
					if($result['count(userID)'] <= 1) {
						//print_r($result);
						$query->closeCursor();
						$query = $this->con->prepare("SELECT * FROM `".($this->pref)."achievements` WHERE `ID` = 5");
						$query->setFetchMode(PDO::FETCH_ASSOC);
						$query->execute();
						if($ach = $query->fetch())
						{
							$query->closeCursor();
							return $ach;
						}
					}
				}
			}
			//Achievement - complete a seafood recipe
			if($player && $category == 4) {
				//echo $category;
				$query = $this->con->prepare("SELECT count(userID) FROM `".($this->pref)."user_posts` as user_posts LEFT JOIN `".($this->pref)."posts_directory` as post_directory ON post_directory.post_id = user_posts.post_id WHERE user_posts.userID = ? AND post_directory.category_id = ?");
				$query->setFetchMode(PDO::FETCH_ASSOC);
				$query->execute(array($player, $category));

				if($results = $query->fetch())
					{
					//print_r($results);
					if($results['count(userID)'] == 1) {
						//echo 'test';
						$query->closeCursor();
						$query = $this->con->prepare("SELECT * FROM `".($this->pref)."achievements` WHERE `ID` = 6");
						$query->setFetchMode(PDO::FETCH_ASSOC);
						$query->execute();
						if($ach = $query->fetch())
						{
							$query->closeCursor();
							return $ach;
						}
					}
				}
			}
			//Achievement - complete a vegetarian recipe
			if($player && $category == 5) {
				//echo $category;
				$query = $this->con->prepare("SELECT count(userID) FROM `".($this->pref)."user_posts` as user_posts LEFT JOIN `".($this->pref)."posts_directory` as post_directory ON post_directory.post_id = user_posts.post_id WHERE user_posts.userID = ? AND post_directory.category_id = ?");
				$query->setFetchMode(PDO::FETCH_ASSOC);
				$query->execute(array($player, $category));

				if($results = $query->fetch())
					{
					//print_r($results);
					if($results['count(userID)'] == 1) {
						//echo 'test';
						$query->closeCursor();
						$query = $this->con->prepare("SELECT * FROM `".($this->pref)."achievements` WHERE `ID` = 10");
						$query->setFetchMode(PDO::FETCH_ASSOC);
						$query->execute();
						if($ach = $query->fetch())
						{
							$query->closeCursor();
							return $ach;
						}
					}
				}
			}
			//Achievement - Certified baker
			if($player && $category == 3) {
				$query = $this->con->prepare("SELECT * FROM `".($this->pref)."user_posts` as user_posts LEFT JOIN `".($this->pref)."posts_directory` as post_directory ON post_directory.post_id = user_posts.post_id WHERE user_posts.userID = ? AND post_directory.category_id = ?");
				$query->setFetchMode(PDO::FETCH_ASSOC);
				$query->execute(array($player, $category));

				if($results = $query->fetchAll())
					{
					if(count($results) == 3) {
						//print_r($results);
						$query->closeCursor();
						$query = $this->con->prepare("SELECT * FROM `".($this->pref)."achievements` WHERE `ID` = 9");
						$query->setFetchMode(PDO::FETCH_ASSOC);
						$query->execute();
						if($ach = $query->fetch())
						{
							$query->closeCursor();
							return $ach;
						}
					}
				}
			}
			//Achievement - reach level 1
			if($player) {
				$query = $this->con->prepare("SELECT * FROM `".($this->pref)."user_stats` as user_stats WHERE user_stats.userID = ?");
				$query->setFetchMode(PDO::FETCH_ASSOC);
				$query->execute(array($player));

				if($results = $query->fetch())
					{
					//print_r($results);
					if($results['level'] == 1) {
						//print_r($results);
						$query->closeCursor();
						$query = $this->con->prepare("SELECT * FROM `".($this->pref)."users_ach` WHERE userID = ? AND `achID` = 11");
						$query->setFetchMode(PDO::FETCH_ASSOC);
						$query->execute(array($results['userID']));

						if($achievement = !$query->fetch()) {
							//print_r($achievement);
							$query->closeCursor();
							$query = $this->con->prepare("SELECT * FROM `".($this->pref)."achievements` LEFT JOIN `".($this->pref)."user_rewards` ON `".($this->pref)."achievements`.ID = `".($this->pref)."user_rewards`.achID WHERE `ID` = 11");
							$query->setFetchMode(PDO::FETCH_ASSOC);
							$query->execute();
							if($ach = $query->fetch())
							{
								$query->closeCursor();
								return $ach;
							}
						}
					}
				}
			}
			//Achievement - reach level 1
			if($player) {
				$query = $this->con->prepare("SELECT * FROM `".($this->pref)."user_stats` as user_stats WHERE user_stats.userID = ?");
				$query->setFetchMode(PDO::FETCH_ASSOC);
				$query->execute(array($player));

				if($results = $query->fetch())
					{
					//print_r($results);
					if($results['experience'] >= 500) {
						//print_r($results);
						$query->closeCursor();
						$query = $this->con->prepare("SELECT * FROM `".($this->pref)."users_ach` WHERE userID = ? AND `achID` = 12");
						$query->setFetchMode(PDO::FETCH_ASSOC);
						$query->execute(array($results['userID']));

						if($achievement = !$query->fetch()) {
							//print_r($achievement);
							$query->closeCursor();
							$query = $this->con->prepare("SELECT * FROM `".($this->pref)."achievements` WHERE `ID` = 12");
							$query->setFetchMode(PDO::FETCH_ASSOC);
							$query->execute();
							if($ach = $query->fetch())
							{
								$query->closeCursor();
								return $ach;
							}
						}
					}
				}
			}
			//Achievement - reach level 3
			if($player) {
				$query = $this->con->prepare("SELECT * FROM `".($this->pref)."user_stats` as user_stats WHERE user_stats.userID = ?");
				$query->setFetchMode(PDO::FETCH_ASSOC);
				$query->execute(array($player));

				if($results = $query->fetch())
					{
					//print_r($results);
					if($results['level'] == 3) {
						print_r($results);
						$query->closeCursor();
						$query = $this->con->prepare("SELECT * FROM `".($this->pref)."users_ach` WHERE userID = ? AND `achID` = 8");
						$query->setFetchMode(PDO::FETCH_ASSOC);
						$query->execute(array($results['userID']));

						if($achievement = !$query->fetch()) {
							//print_r($achievement);
							$query->closeCursor();
							$query = $this->con->prepare("SELECT * FROM `".($this->pref)."achievements` WHERE `ID` = 8");
							$query->setFetchMode(PDO::FETCH_ASSOC);
							$query->execute();
							if($ach = $query->fetch())
							{
								$query->closeCursor();
								return $ach;
							}
						}
					}
				}
			}
		}
	catch(PDOException $e) {
		$this->err[] = 'Error : '.$e->getMessage();
	}
}

	//display completed recipes in profile area - NB
	public function recipe_history($userID) {
		try {
			//select all recipes that the user has completed
			$query = $this->con->prepare("SELECT * FROM `".($this->pref)."user_posts` as user_posts INNER JOIN `".($this->pref)."posts` recipes ON user_posts.post_id = recipes.post_id WHERE `userID` = ?");	
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query->execute(array($userID));

			if($results = $query->fetchAll()) {
				$query->closeCursor();
				return $results;
			}

		}
		catch(PDOException $e) {
			$this->err[] = 'Error : '.$e->getMessage();
		}
	}

	//allow users to create their own recipes - NB
	public function create_recipe($author, $post_title, $post_slug, $category, $description, $recipe_photo, $post_date, $post_experience, $recipe_level, $difficulty, $post_source, $ingredients, $method, $nutritional_info, $time, $quantity, $calories) {
		try {
			//$query = $this->con->prepare("INSERT INTO `".($this->pref)."posts` SET `author` = '$author', `post_title` = '$post_title', `post_slug` = '$post_slug', `post_content` = '$description', `post_photo` = '$recipe_photo', `post_date` = '$post_date', `post_experience` = '$post_experience', `difficulty` = '$difficulty', `post_source` = '$post_source'");	
			//$query->setFetchMode(PDO::FETCH_ASSOC);
			$query = $this->con->prepare("INSERT INTO `".($this->pref)."posts` (author, post_title, post_slug, post_content, post_photo, post_date, post_experience, post_level, difficulty, post_source) VALUES ('$author', '$post_title', '$post_slug', '$description', '$recipe_photo', '$post_date', '$post_experience', '$recipe_level', '$difficulty', '$post_source')");
			$query->execute(array($author, $post_title, $post_slug, $description, $recipe_photo, $post_date, $post_experience, $difficulty, $recipe_level, $post_source));
			$recipe = true;
			
			if($recipe) {
				$query->closeCursor();
				$query = $this->con->prepare("SELECT * FROM `".($this->pref)."posts` WHERE `post_slug` = '$post_slug'");	
				$query->setFetchMode(PDO::FETCH_ASSOC);
				$query->execute(array($author, $post_title, $post_slug, $description, $recipe_photo, $post_date, $post_experience, $difficulty, $post_source));

			if($results = $query->fetch()) {
				$post_id = $results['post_id'];
				$query = $this->con->prepare("INSERT INTO `".($this->pref)."post_schema` SET `post_id` = '$post_id', `ingredients` = '$ingredients', `method` = '$method', `nutritional_info` = '$nutritional_info', `time` = '$time', `quantity` = '$quantity', `calories` = '$calories'");
				$query->execute(array($ingredients, $method, $nutritional_info, $time, $quantity, $calories));
				$recipe_schema = true;

				$query->closeCursor();
				$query = $this->con->prepare("SELECT * FROM `".($this->pref)."post_schema` WHERE `post_id` = '$post_id'");	
				$query->setFetchMode(PDO::FETCH_ASSOC);
				$query->execute(array($post_id));
			

				if($schema_results = $query->fetch()) {
					//print_r($schema_results);
					$country_id = '22';
					$schema_id = $schema_results['schema_id'];
					$query = $this->con->prepare("INSERT INTO `".($this->pref)."posts_directory` SET `post_id` = '$post_id', `category_id` = '$category', `country_id` = '$country_id', `schema_id` = '$schema_id'");
					$query->execute(array($post_id, $category, $country_id, $time, $quantity, $calories));
						}
					}
				}
			}

		catch(PDOException $e) {
			$this->err[] = 'Error : '.$e->getMessage();
		}
	}

	//times of completed actions for achievements
	public function action($username, $achievement, $amount = 1){
		try{
			$query = $this->con->prepare("SELECT * FROM `".($this->pref)."user_stats` WHERE `playername` = ?");
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query->execute(array($username));
			if($user = $query->fetch())
			{
				$query->closeCursor();
				$query = $this->con->prepare("SELECT * FROM `".($this->pref)."achievements` WHERE `ID` = ?");
				$query->setFetchMode(PDO::FETCH_ASSOC);
				$query->execute(array($achievement));
				if($ach = $query->fetch())
				{
					//print_r($ach);
					//checking if achievement is enabled
					if($ach["status"] == "active")
					{
						$now = time();
						$complete = false;
						$query->closeCursor();
						$query = $this->con->prepare("SELECT * FROM `".($this->pref)."users_ach` WHERE `userID` = ? and `achID` = ?");
						$query->setFetchMode(PDO::FETCH_ASSOC);
						$query->execute(array($user["userID"], $ach["ID"]));
						if($rel = $query->fetch())
						{
							$query->closeCursor();
							//checking if achievement is not completed yet
							if($rel["status"] == "active")
							{
								$amount += $rel["amount"];
								//checking if needed period of time is passed
								if($now >= $rel["last_time"] + $ach["time_period"])
								{
									//checking if no we have completed an achievement
									if($amount >= $ach["amount_needed"])
									{
										//complete achievement
										$query = $this->con->prepare("UPDATE `".($this->pref)."users_ach` SET `amount` = ?, `status` = 'completed', `last_time` = ? WHERE ID = ?");
										$complete = true;
									}
									else
									{
										//update existing relation
										$query = $this->con->prepare("UPDATE `".($this->pref)."users_ach` SET `amount` = ?, `last_time` = ? WHERE `ID` = ?");
									}
									$query->execute(array($amount, $now, $rel["ID"]));
									$query->closeCursor();
									if($complete)
									{
										return $ach;
									}
								}
							}
						}
						else
						{
							//print_r($user);
							//print_r($ach);
							$query->closeCursor();
							$status = "active";
							if($amount >= $ach["amount_needed"])
							{
								$status = "completed";
								$complete = true;
							}
							//create relation
							$query = $this->con->prepare("INSERT INTO `".($this->pref)."users_ach` SET `userID` = ?, `achID` = ?, `amount` = ?, `last_time` = ?, `status` = ?");
							//$user["ID"] to $user["userID"]
							$query->execute(array($user["userID"], $ach["ID"], $amount, $now, $status));
							$query->closeCursor();
							if($complete)
							{
								return $ach;
							}
						}
					}
				}
				else
				{	
					$this->err[] = "Achievement with ID ".$achievement." does not exist";
				}
			}
			else
			{	
				//user doesn't exist, create user and perform action
				$this->create_user($username);
				$this->action($username, $achievement, $amount);
			}
		}
		catch(PDOException $e) {
			$this->err[] = 'Error : '.$e->getMessage();
		}
		return false;
	}
	
	//free resources
	function __destruct(){
		$this->con = NULL;
	}
}
?>